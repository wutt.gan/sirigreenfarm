<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TypeProductsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('type_products')->delete();
        
        \DB::table('type_products')->insert(array (
            0 => 
            array (
                'id' => 1,
                'code' => 'SL',
                'name_th' => 'ผักสลัด',
                'record_status' => 1,
                'created_at' => '2022-07-12 15:05:58',
                'updated_at' => '2022-07-12 15:05:58',
            ),
            1 => 
            array (
                'id' => 2,
                'code' => 'VT',
                'name_th' => 'ผักไทย',
                'record_status' => 1,
                'created_at' => '2022-07-12 15:05:58',
                'updated_at' => '2022-07-12 15:05:58',
            ),
            2 => 
            array (
                'id' => 3,
                'code' => 'FL',
                'name_th' => 'ผลไม้',
                'record_status' => 1,
                'created_at' => '2022-07-12 15:05:58',
                'updated_at' => '2022-07-12 15:05:58',
            ),
            3 => 
            array (
                'id' => 4,
                'code' => 'CC',
                'name_th' => 'ถ่าน',
                'record_status' => 1,
                'created_at' => '2022-07-12 15:05:58',
                'updated_at' => '2022-07-12 15:05:58',
            ),
            4 => 
            array (
                'id' => 5,
                'code' => 'TM',
            'name_th' => 'สินค้าแปรรูป (missveggie)',
                'record_status' => 1,
                'created_at' => '2022-07-12 15:05:58',
                'updated_at' => '2022-07-12 15:05:58',
            ),
            5 => 
            array (
                'id' => 6,
                'code' => 'TO',
            'name_th' => 'สินค้าแปรรูป (อื่นๆ)',
                'record_status' => 1,
                'created_at' => '2022-07-12 15:05:58',
                'updated_at' => '2022-07-12 15:05:58',
            ),
        ));
        
        
    }
}
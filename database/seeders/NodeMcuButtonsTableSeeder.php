<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class NodeMcuButtonsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('node_mcu_buttons')->delete();

        \DB::table('node_mcu_buttons')->insert(array (
            0 =>
            array (
                'id' => 1,
                'id_node' => 'siri_farm_t_01',
                'code_button' => 'bt_led_01',
                'description' => 'ทดสอบปุ่มปิด-เปิด ไฟ',
                'status' => false,
                'record_status' => 1,
                'created_at' => '2023-06-02 15:43:14',
                'updated_at' => '2023-06-02 15:43:14',
            ),
        ));


    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'wutthaihci phanthong',
                'id_role' => 2,
                'email' => 'wuttgan@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$.UQRbQMOJmsb3a0gl06gNO8LPddcZn22vMFgDv/wEXCmK1sB3eF1a',
                'remember_token' => NULL,
                'record_status' => 1,
                'created_at' => '2022-05-10 11:30:42',
                'updated_at' => '2022-05-10 11:30:42',
            ),
        ));
        
        
    }
}
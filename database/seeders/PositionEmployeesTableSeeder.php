<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class PositionEmployeesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('position_employees')->delete();
        
        \DB::table('position_employees')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'ผู้จัดการ',
                'record_status' => 1,
                'created_at' => '2023-01-24 16:05:36',
                'updated_at' => '2023-01-24 16:05:36',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'ธุรการ',
                'record_status' => 1,
                'created_at' => '2023-01-24 16:05:36',
                'updated_at' => '2023-01-24 16:05:36',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'บัญชี',
                'record_status' => 1,
                'created_at' => '2023-01-24 16:05:36',
                'updated_at' => '2023-01-24 16:05:36',
            ),
            3 => 
            array (
                'id' => 4,
            'name' => 'ฝ่ายผลิต(ถ่าน)',
                'record_status' => 1,
                'created_at' => '2023-01-24 16:05:36',
                'updated_at' => '2023-01-24 16:05:36',
            ),
            4 => 
            array (
                'id' => 5,
            'name' => 'ฝ่ายผลิต(ไฮโดร)',
                'record_status' => 1,
                'created_at' => '2023-01-24 16:05:36',
                'updated_at' => '2023-01-24 16:05:36',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'ฝ่ายจัดเตรียม',
                'record_status' => 1,
                'created_at' => '2023-01-24 16:05:36',
                'updated_at' => '2023-01-24 16:05:36',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'พนักงานขับรถ',
                'record_status' => 1,
                'created_at' => '2023-01-24 16:05:36',
                'updated_at' => '2023-01-24 16:05:36',
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'ช่าง',
                'record_status' => 1,
                'created_at' => '2023-01-24 16:05:36',
                'updated_at' => '2023-01-24 16:05:36',
            ),
        ));
        
        
    }
}
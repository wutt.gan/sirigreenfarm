<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class BanksTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('banks')->delete();
        
        \DB::table('banks')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'ธนาคารไทยพาณิชย์',
                'record_status' => 1,
                'created_at' => '2023-01-25 14:34:31',
                'updated_at' => '2023-01-25 14:34:31',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'ธนาคารกรุงศรีอยุธยา',
                'record_status' => 1,
                'created_at' => '2023-01-25 14:34:31',
                'updated_at' => '2023-01-25 14:34:31',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'ธนาคารออมสิน',
                'record_status' => 1,
                'created_at' => '2023-01-25 14:34:31',
                'updated_at' => '2023-01-25 14:34:31',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'ธนาคารกสิกรไทย',
                'record_status' => 1,
                'created_at' => '2023-01-25 14:34:31',
                'updated_at' => '2023-01-25 14:34:31',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'ธนาคารกรุงไทย',
                'record_status' => 1,
                'created_at' => '2023-01-25 14:34:31',
                'updated_at' => '2023-01-25 14:34:31',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'ธนาคารกรุงเทพ',
                'record_status' => 1,
                'created_at' => '2023-01-25 14:34:31',
                'updated_at' => '2023-01-25 14:34:31',
            ),
        ));
        
        
    }
}
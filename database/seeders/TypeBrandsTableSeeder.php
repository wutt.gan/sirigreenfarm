<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TypeBrandsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('type_brands')->delete();
        
        \DB::table('type_brands')->insert(array (
            0 => 
            array (
                'id' => 1,
                'id_product_type' => 2,
                'id_product_brand' => 2,
            ),
            1 => 
            array (
                'id' => 2,
                'id_product_type' => 2,
                'id_product_brand' => 3,
            ),
            2 => 
            array (
                'id' => 3,
                'id_product_type' => 2,
                'id_product_brand' => 4,
            ),
            3 => 
            array (
                'id' => 4,
                'id_product_type' => 6,
                'id_product_brand' => 5,
            ),
            4 => 
            array (
                'id' => 5,
                'id_product_type' => 1,
                'id_product_brand' => 7,
            ),
            5 => 
            array (
                'id' => 6,
                'id_product_type' => 6,
                'id_product_brand' => 8,
            ),
            6 => 
            array (
                'id' => 7,
                'id_product_type' => 5,
                'id_product_brand' => 6,
            ),
            7 => 
            array (
                'id' => 8,
                'id_product_type' => 5,
                'id_product_brand' => 2,
            ),
            8 => 
            array (
                'id' => 9,
                'id_product_type' => 4,
                'id_product_brand' => 9,
            ),
        ));
        
        
    }
}
<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class NodeMcuKeysTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('node_mcu_keys')->delete();
        
        \DB::table('node_mcu_keys')->insert(array (
            0 => 
            array (
                'id' => 1,
                'id_node' => 'siri_farm_t_01',
                'id_key' => '8x2f9j4n3b6i7g5t0h1yq7w2e4r5t6y8u9',
                'created_at' => '2023-06-05 17:18:58',
                'updated_at' => '2023-06-05 17:18:58',
            ),
        ));
        
        
    }
}
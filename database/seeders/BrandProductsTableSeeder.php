<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class BrandProductsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('brand_products')->delete();
        
        \DB::table('brand_products')->insert(array (
            0 => 
            array (
                'id' => 1,
                'code' => 'OH',
                'name_th' => 'ทั่วไป',
                'record_status' => 1,
                'created_at' => '2022-09-22 11:48:04',
                'updated_at' => '2022-09-22 11:48:04',
            ),
            1 => 
            array (
                'id' => 2,
                'code' => 'CB',
                'name_th' => 'C\'EST BON',
                'record_status' => 1,
                'created_at' => '2022-09-22 11:48:04',
                'updated_at' => '2022-09-22 11:48:04',
            ),
            2 => 
            array (
                'id' => 3,
                'code' => 'PK',
                'name_th' => 'โครงการหลวง',
                'record_status' => 1,
                'created_at' => '2022-09-22 11:48:04',
                'updated_at' => '2022-09-22 11:48:04',
            ),
            3 => 
            array (
                'id' => 4,
                'code' => 'NT',
                'name_th' => 'ปลอดสารพิษ',
                'record_status' => 1,
                'created_at' => '2022-09-22 11:48:04',
                'updated_at' => '2022-09-22 11:48:04',
            ),
            4 => 
            array (
                'id' => 5,
                'code' => 'DY',
                'name_th' => 'DONOYA',
                'record_status' => 1,
                'created_at' => '2022-09-22 11:48:04',
                'updated_at' => '2022-09-22 11:48:04',
            ),
            5 => 
            array (
                'id' => 6,
                'code' => 'MV',
                'name_th' => 'MISS VEGGIE',
                'record_status' => 1,
                'created_at' => '2022-09-30 13:36:50',
                'updated_at' => '2022-09-30 13:36:50',
            ),
            6 => 
            array (
                'id' => 7,
                'code' => 'SF',
                'name_th' => 'SIRIGREENFARM',
                'record_status' => 1,
                'created_at' => '2022-09-30 13:38:05',
                'updated_at' => '2022-09-30 13:38:05',
            ),
            7 => 
            array (
                'id' => 8,
                'code' => 'MK',
                'name_th' => 'MIND KOMBUCHA',
                'record_status' => 1,
                'created_at' => '2022-09-30 14:17:20',
                'updated_at' => '2022-09-30 14:17:20',
            ),
            8 => 
            array (
                'id' => 9,
                'code' => 'CF',
                'name_th' => 'C\'FIRE',
                'record_status' => 1,
                'created_at' => '2022-10-19 16:53:02',
                'updated_at' => '2022-10-19 16:53:02',
            ),
        ));
        
        
    }
}
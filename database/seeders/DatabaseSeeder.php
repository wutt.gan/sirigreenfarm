<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call(UsersTableSeeder::class);
        $this->call(TypeProductsTableSeeder::class);
        $this->call(UnitProductsTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
        $this->call(BrandProductsTableSeeder::class);
        $this->call(TypeBrandsTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(TypePlacesTableSeeder::class);
        $this->call(PositionEmployeesTableSeeder::class);
        $this->call(BanksTableSeeder::class);
        $this->call(NodeMcuButtonsTableSeeder::class);
        $this->call(NodeMcuSensersTableSeeder::class);
        $this->call(NodeMcuKeysTableSeeder::class);
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UnitProductsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('unit_products')->delete();
        
        \DB::table('unit_products')->insert(array (
            0 => 
            array (
                'id' => 1,
                'code' => 'KK',
                'name_th' => 'กก.',
                'record_status' => 1,
                'created_at' => '2022-07-12 15:01:09',
                'updated_at' => '2022-07-12 15:01:09',
            ),
            1 => 
            array (
                'id' => 2,
                'code' => 'PP',
                'name_th' => 'ลูก',
                'record_status' => 1,
                'created_at' => '2022-07-12 15:01:09',
                'updated_at' => '2022-07-12 15:01:09',
            ),
            2 => 
            array (
                'id' => 3,
                'code' => 'BG',
                'name_th' => 'ถุง',
                'record_status' => 1,
                'created_at' => '2022-07-12 15:01:09',
                'updated_at' => '2022-07-12 15:01:09',
            ),
            3 => 
            array (
                'id' => 4,
                'code' => 'BT',
                'name_th' => 'ขวด',
                'record_status' => 1,
                'created_at' => '2022-09-30 14:13:59',
                'updated_at' => '2022-09-30 14:13:59',
            ),
            4 => 
            array (
                'id' => 5,
                'code' => 'PK',
                'name_th' => 'แพ็ค',
                'record_status' => 1,
                'created_at' => '2022-09-30 14:13:59',
                'updated_at' => '2022-09-30 14:13:59',
            ),
            5 => 
            array (
                'id' => 6,
                'code' => 'CH',
                'name_th' => 'ช่อ',
                'record_status' => 1,
                'created_at' => '2022-10-07 13:40:11',
                'updated_at' => '2022-10-07 13:40:11',
            ),
            6 => 
            array (
                'id' => 7,
                'code' => 'BO',
                'name_th' => 'กล่อง',
                'record_status' => 1,
                'created_at' => '2022-10-07 13:41:00',
                'updated_at' => '2022-10-07 13:41:00',
            ),
        ));
        
        
    }
}
<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class NodeMcuSensersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('node_mcu_sensers')->delete();
        
        \DB::table('node_mcu_sensers')->insert(array (
            0 => 
            array (
                'id' => 1,
                'id_node' => 'siri_farm_t_01',
                'code_senser' => 'tem_e_01',
                'status' => 0,
                'description' => 'ตัววัดอุหภูมิ พื้นดิน',
                'record_status' => 1,
                'created_at' => '2023-06-02 15:47:10',
                'updated_at' => '2023-06-02 15:47:10',
            ),
            1 => 
            array (
                'id' => 2,
                'id_node' => 'siri_farm_t_01',
                'code_senser' => 'hum_e_01',
                'status' => 0,
                'description' => 'ตัววัดความชื้นในอากาศ',
                'record_status' => 1,
                'created_at' => '2023-06-02 15:51:44',
                'updated_at' => '2023-06-02 15:51:44',
            ),
            2 => 
            array (
                'id' => 3,
                'id_node' => 'siri_farm_t_01',
                'code_senser' => 'ph_e_01',
                'status' => 0,
                'description' => 'ตัววัด ph',
                'record_status' => 1,
                'created_at' => '2023-06-02 15:51:44',
                'updated_at' => '2023-06-02 15:51:44',
            ),
        ));
        
        
    }
}
<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('roles')->delete();
        
        \DB::table('roles')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'admin',
                'record_status' => 1,
                'created_at' => '2022-12-20 15:06:35',
                'updated_at' => '2022-12-20 15:06:35',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'hr',
                'record_status' => 1,
                'created_at' => '2022-12-20 15:06:35',
                'updated_at' => '2022-12-20 15:06:35',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'seller',
                'record_status' => 1,
                'created_at' => '2022-12-20 15:06:35',
                'updated_at' => '2022-12-20 15:06:35',
            ),
        ));
        
        
    }
}
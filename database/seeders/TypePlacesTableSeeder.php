<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TypePlacesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('type_places')->delete();
        
        \DB::table('type_places')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'กทม.',
                'record_status' => 1,
                'created_at' => '2023-01-24 16:04:34',
                'updated_at' => '2023-01-24 16:04:34',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'สวนป่า',
                'record_status' => 1,
                'created_at' => '2023-01-24 16:04:34',
                'updated_at' => '2023-01-24 16:04:34',
            ),
        ));
        
        
    }
}
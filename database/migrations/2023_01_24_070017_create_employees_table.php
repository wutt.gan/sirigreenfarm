<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->id();
            $table->tinyInteger('title')->comment('1=นาย,2=นาง,3=นางสาว');
            $table->integer('id_type_place');
            $table->integer('id_position');
            $table->integer('id_bank');
            $table->string('code_id',20);
            $table->string('full_name_th',200);
            $table->string('full_name_en',200);
            $table->string('nick_name',50);
            // $table->tinyInteger('gender')->comment('1=ชาย,2=หญิง');\
            $table->string('email',200)->unique();
            $table->string('tel',12);
            $table->string('line',20);
            $table->date('date_work');
            $table->string('code_bank',15);
            $table->string('card_pdf',200)->nullable();
            $table->tinyInteger('status')->default(1);
            $table->tinyInteger('record_status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}

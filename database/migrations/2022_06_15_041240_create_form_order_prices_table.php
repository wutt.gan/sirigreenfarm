<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormOrderPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_order_prices', function (Blueprint $table) {
            $table->id();
            $table->string('fullname',200);
            $table->string('email',200);
            $table->string('line',100);
            $table->string('tel',13);
            $table->string('note',600);
            $table->string('remark',600)->nullable();
            $table->tinyInteger('status')->default(1);
            $table->string('id_user_confirm')->nullable();
            $table->tinyInteger('record_status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_order_prices');
    }
}

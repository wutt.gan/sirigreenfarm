<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_product_type');
            $table->integer('id_product_brand');
            $table->integer('id_product_unit');
            $table->string('code',20);
            $table->string('name_th');
            $table->string('name_en')->nullable();
            $table->integer('amount')->default(0);
            $table->integer('price')->default(0);
            $table->string('image');
            $table->string('discription',1000)->nullable();
            $table->tinyInteger('status')->default(1);
            $table->tinyInteger('record_status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}

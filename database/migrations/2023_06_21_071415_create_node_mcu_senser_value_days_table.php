<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNodeMcuSenserValueDaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('node_mcu_senser_value_days', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_node_senser');
            $table->float('value',10,2);
            $table->dateTime('date');
            $table->tinyInteger('record_status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('node_mcu_senser_value_days');
    }
}

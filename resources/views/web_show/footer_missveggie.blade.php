    <!-- Footer Start -->
    <div class="container-fluid bg-dark text-light mt-5 wow fadeInUp" data-wow-delay="0.1s" id="contact">
        <div class="container">
            <div class="row gx-5">
                <div class="col-lg-4 col-md-6 footer-about">
                    <div class="d-flex flex-column align-items-center justify-content-center text-center h-100 bg-success p-4">
                        <a href="#" class="navbar-brand">
                            <img src="{{asset("/img/missveggie_circle.png")}}" style="width: 65px;margin-top: 1px;" alt="MISSVEGGIE">
                            <br>
                            <h3 class="m-0 text-white FC_Ekaluck">MISSVEGGIE</h3>
                        </a>
                        <p class="mt-3 mb-4 FC_Ekaluck">จำหน่ายผักสลัด สลัดพร้อมทาน สลัดกล่อง สินค้าแปรรูปเกี่ยวกับเกษตร</p>

                    </div>
                </div>
                <div class="col-lg-8 col-md-6">
                    <div class="row gx-5">
                        <div class="col-lg-6 col-md-12 pt-5 mb-5">
                            <div class="section-title section-title-sm position-relative pb-3 mb-4">
                                <h3 class="text-light mb-0 FC_Ekaluck">ติดต่อเรา</h3>
                            </div>
                            <div class="d-flex mb-2">
                                <i class="bi bi-geo-alt text-success me-2"></i>
                                <p class="mb-0 FC_Ekaluck">ภายในอาคารอินเตอร์เชนจ์ 21</p>
                            </div>
                            <div class="d-flex mb-2">
                                <i class="bi bi-envelope-open text-success me-2"></i>
                                <p class="mb-0 FC_Ekaluck">Email: sirigreenfarm4656@gmail.com</p>
                            </div>
                            <div class="d-flex mb-2">
                                <i class="bi bi-telephone text-success me-2"></i>
                                <p class="mb-0 FC_Ekaluck">Tel: 062-2496424, 092-7409129</p>
                            </div>
                            <div class="d-flex mb-2">
                                <i class="fab fa-line text-success me-2"></i>
                                <p class="mb-0 FC_Ekaluck">Line Id: 0622496424</p>
                            </div>
                            <div class="d-flex mt-4">
                                {{-- <a class="btn btn-success btn-square me-2" href="#"><i class="fab fa-twitter fw-normal"></i></a> --}}
                                <a class="btn btn-success btn-square me-2" href="https://www.facebook.com/MissVeggiebySiriGreenFarm" target="_blank"><i class="fab fa-facebook-f fw-normal"></i></a>
                                <a class="btn btn-success btn-square me-2" href="#"><i class="fab fa-linkedin-in fw-normal"></i></a>
                                <a class="btn btn-success btn-square" href="#"><i class="fab fa-instagram fw-normal"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12 pt-0 pt-lg-5 mb-5">
                            <div class="section-title section-title-sm position-relative pb-3 mb-4">
                                <h3 class="text-light mb-0 FC_Ekaluck">ที่ตั้งร้าน</h3>
                            </div>
                            <div class="link-animated d-flex flex-column justify-content-start">
                                <div id="map"></div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid text-white" style="background: #065310;">
        <div class="container text-center">
            <div class="row justify-content-end">
                <div class="col-lg-8 col-md-6">
                    <div class="d-flex align-items-center justify-content-center" style="height: 75px;">
                        <p class="mb-0"> All Rights Reserved.

						<!--/*** This template is free as long as you keep the footer author’s credit link/attribution link/backlink. If you'd like to use the template without the footer author’s credit link/attribution link/backlink, you can purchase the Credit Removal License from "https://htmlcodex.com/credit-removal". Thank you for your support. ***/-->
						Designed by <a class="text-white border-bottom" href="https://htmlcodex.com">HTML Codex</a>

                        Distributed By: <a class="text-white border-bottom" href="https://themewagon.com" target="_blank">ThemeWagon</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer End -->

<!DOCTYPE html>
<html lang="en">

<!-- Meta Pixel Code -->
<script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window, document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '808494340360563');
    fbq('track', 'PageView');
</script>

    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=808494340360563&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Meta Pixel Code -->

<head>
    <meta charset="utf-8">
    <title>SIRIGREENFARM ฟาร์มผักสลัดไฮโดรโปนิกส์ บริการจัดซื้อ-จัดหา ผักไทย ผลไม้ ถ่านกะลาอัดแท่ง ถ่านไม้ยูคา พระราม3</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="SIRIGREENFARM, ฟาร์มผักสลัดไฮโดรโปนิกส์ พระราม3, ฟาร์มผักสลัดไฮโดรโปนิกส์ ด่านช้าง, ฟาร์มผักสลัดไฮโดรโปนิกส์, บริการจัดซื้อ-จัดหา ผักสลัดไฮโดรโปนิกส์,
    บริการจัดซื้อ-จัดหา ผักทั่วไป, บริการจัดซื้อ-จัดหา ผลไม้, ถ่านกะลาอัดแท่ง, ถ่านกะลาอัดแท่ง พระราม3, ถ่านไม้ยูคา, ถ่านไม้ยูคา พระราม3,
    ถ่านกะลาอัดแท่ง ด่านช้าง, ถ่านไม้ยูคา ด่านช้าง, อาหารเพื่อสุขภาพ" name="keywords">
    <meta content="SIRIGREENFARM ผักสลัดไฮโดรโปนิกส์จากฟาร์มสุพรรณ ด่านช้าง ผักสลัดไฮโดรโปนิกส์พระราม3 บริการจัดซื้อ-จัดหา ผักทั่วไป ผลไม้ ถ่านกะลาอัดแท่ง ถ่านไม้ยูคา พระราม3" name="description">

    <link rel="canonical" href="https://www.sirigreenfarm.com" />

    <!-- Favicon -->
    <link rel="icon" href="{{ asset('/img/logo_circle.png') }}">

    <!-- Google Web Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700;800&family=Rubik:wght@400;500;600;700&display=swap"
        rel="stylesheet">

    <!-- Icon Font Stylesheet -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">

    <!-- Libraries Stylesheet -->
    <link href={{ asset('lib/owlcarousel/assets/owl.carousel.min.css') }} rel="stylesheet">
    <link href={{ asset('lib/animate/animate.min.css') }} rel="stylesheet">

    <!-- Customized Bootstrap Stylesheet -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Template Stylesheet -->
    <link href="css/style.css" rel="stylesheet">

    <link href="css/home.css" rel="stylesheet">

    <style>
        body {
            font-family: FC_Ekaluck;
        }

        .bg-primary {
            background-color: #0a9a1d !important;
        }

        #map {
            height: 200px;
            /* The height is 400 pixels */
            width: 100%;
            /* The width is the width of the web page */
        }

        .display-none {
            display: none;
        }
    </style>

    <script src="{{ asset('js/app.js') }}" defer></script>



</head>

<body>

    @include('web_show.menu_home')
    @include('web_show.cookie_agree')

    <div id="app">

        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                    <h5 class="modal-title FC_Ekaluck" id="exampleModalLabel">ใบเสนอราคา</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                            <from-order-price></from-order-price>
                    </div>
                </div>
            </div>
        </div>


        <div id="header-carousel" class="carousel slide carousel-fade" data-bs-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img width="100%" height="55%" class="" src="img/carousel_top-1.jpg" alt="ฟาร์มผักสลัดไฮโดรโปนิกส์, ฟาร์มผักสลัดไฮโดรโปนิกส์ พระราม3, ฟาร์มผักสลัดไฮโดรโปนิกส์ ด่านช้าง, ผักสลัด, อาหารเพื่อสุขภาพ">
                    <div class="carousel-caption  d-flex flex-column align-items-center justify-content-center">
                        <div class="p-3 " style="max-width: 900px;">
                            <h3 class="d-none d-md-block text-white text-uppercase mb-3 animated slideInDown FC_Ekaluck">ฟาร์มผักสลัด</h3>
                            <h1 class="FC_Ekaluck display-1 text-white mb-md-4 animated zoomIn ">
                                ฟาร์มผักสลัดไฮโดรโปนิกส์
                            </h1>
                            <a href="/farm"class="btn btn-outline-light btn-sm py-md-3 px-md-5 animated slideInRight FC_Ekaluck">รายละเอียด</a>
                            <button
                                class="btn btn-success btn-outline-light btn-sm py-md-3 px-md-5 animated slideInRight FC_Ekaluck"
                                data-bs-toggle="modal" data-bs-target="#exampleModal"
                                style="background-color: #008400;"
                            >ใบเสนอราคา
                            </button>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <img width="100%" height="55%" class="" src="img/carousel_top-2.jpg" alt="missveggie, ผักสลัดแปรรูปจากฟาร์ม, ผักสลัดจากฟาร์ม, ร้านขายผักพระราม3">
                    <div class="carousel-caption  d-flex flex-column align-items-center justify-content-center">
                        <div class="p-3 " style="max-width: 900px;">
                            <h3 class="d-none d-md-block text-white text-uppercase mb-3 animated slideInDown FC_Ekaluck">ผักสลัดแปรรูปจากฟาร์ม
                            </h3>
                            <h1 class="display-1 text-white mb-md-4 animated zoomIn FC_Ekaluck">Miss Veggie</h1>
                            <a href="/missveggie"
                                class="btn btn-outline-light btn-sm py-md-3 px-md-5 animated slideInRight FC_Ekaluck">รายละเอียด</a>
                            <button
                                class="btn btn-success btn-outline-light btn-sm py-md-3 px-md-5 animated slideInRight FC_Ekaluck"
                                data-bs-toggle="modal" data-bs-target="#exampleModal"
                                style="background-color: #008400;"
                            >ใบเสนอราคา
                            </button>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <img width="100%" height="55%" class="" src="img/carousel_top-3.jpg" alt="จัดจำหน่าย, จัดซื้อจัดหา, ผักพื้นบ้าน, ผักไทย, ผลไม้">
                    <div class="carousel-caption  d-flex flex-column align-items-center justify-content-center">
                        <div class="p-3" style="max-width: 900px;">
                            <h3 class="d-none d-md-block text-white text-uppercase mb-3 animated slideInDown FC_Ekaluck">จัดซื้อจัดหา
                                ผักสลัด, ผักทั่วไป, ผักไทย, ผลไม้, ถ่านกะลาอัดแท่ง, ถ่านไม้ยูคา</h3>
                            <h1 class="display-1 text-white mb-md-4 animated zoomIn  FC_Ekaluck">Supplier จัดซื้อ,จัดหาสินค้า
                            </h1>
                            <a href="{{ route('home_supplier') }}"
                                class="btn btn-outline-light btn-sm py-md-3 px-md-5 animated slideInRight FC_Ekaluck">รายละเอียด</a>
                            <button
                                class="btn btn-success btn-outline-light btn-sm py-md-3 px-md-5 animated slideInRight FC_Ekaluck"
                                data-bs-toggle="modal" data-bs-target="#exampleModal"
                                style="background-color: #008400;"
                            >ใบเสนอราคา
                            </button>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <img width="100%" height="55%" class="" src="img/carousel_top-4.jpg" alt="ถ่านกะลาอัดแท่ง, ถ่านไม้ยูคา, ถ่านกะลาอัดแท่ง พระราม3, ถ่านไม้ยูคา พระราม3">
                    <div class="carousel-caption  d-flex flex-column align-items-center justify-content-center">
                        <div class="p-3 " style="max-width: 900px;">
                            <h3 class="d-none d-md-block text-white text-uppercase mb-3 animated slideInDown FC_Ekaluck">ผลิต & จำหน่าย
                            </h3>
                            <h1 class="display-1 text-white mb-md-4 animated zoomIn  FC_Ekaluck">ถ่านกะลาอัดแท่ง,
                                ถ่านไม้ยูคา</h1>

                            <a href="/cfire"
                                class="btn btn-outline-light btn-sm py-md-3 px-md-5 animated slideInRight FC_Ekaluck">รายละเอียด</a>
                            <button
                                class="btn btn-success btn-outline-light btn-sm py-md-3 px-md-5 animated slideInRight FC_Ekaluck"
                                data-bs-toggle="modal" data-bs-target="#exampleModal"
                                style="background-color: #008400;"
                            >ใบเสนอราคา
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#header-carousel" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#header-carousel"
                data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>


        <!-- Facts Start -->
        <div class="container-fluid facts py-5 pt-lg-0">
            <div class="container py-5 pt-lg-0">
                <div class="row gx-0">
                    <div class="col-lg-4 wow zoomIn" data-wow-delay="0.1s">
                        <div class="shadow d-flex align-items-center justify-content-center p-4"
                            style="height: 150px;text-align: center;background-color: #0a9a1d;">

                            <div class="ps-4">
                                <h2 class="text-white mb-0 FC_Ekaluck">ผักสลัดไฮโดรโปนิกส์</h2>
                                {{-- <h5 class="text-white mb-0 FC_Ekaluck" >10 <span>รายการ</span></h5> --}}
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 wow zoomIn" data-wow-delay="0.3s">
                        <div class="shadow d-flex align-items-center justify-content-center p-4"
                            style="height: 150px;text-align: center;background-color: #fcb500;">

                            <div class="ps-4">
                                <h2 class="text-black mb-0 FC_Ekaluck">ผัก & ผลไม้</h2>
                                {{-- <h5 class="text-black mb-0 FC_Ekaluck" >10 <span>รายการ</span></h5> --}}
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 wow zoomIn" data-wow-delay="0.6s">
                        <div class="shadow d-flex align-items-center justify-content-center p-4"
                            style="height: 150px;text-align: center;background-color: #707070;">

                            <div class="ps-4">
                                <h2 class="text-white mb-0 FC_Ekaluck">ถ่านกะลาอัดแท่ง & ถ่านไม้ยูคา</h2>
                                {{-- <h5 class="text-white mb-0 FC_Ekaluck" >1<span>รายการ</span></h5> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Facts Start -->


        <!-- About Start -->
        <div class="container-fluid py-5 wow fadeInUp" data-wow-delay="0.1s" id="about">
            <div class="container py-5">
                <div class="row g-5">
                    <div class="col-lg-7">
                        <div class="section-title position-relative pb-3 mb-5">
                            <h2 class="fw-bold text-success text-uppercase FC_Ekaluck">เกี่ยวกับเรา</h2>
                            <h1 class="mb-0 FC_Ekaluck">บริษัท สิริ กรีน ฟาร์ม จำกัด</h1>
                        </div>
                        <p class="mb-4 FC_Ekaluck" style="text-indent: 2em;">
                                    ดำเนินธุรกิจด้านการทำฟาร์มปลูก แปรรูป และจัดจำหน่าย
                            ผักสลัดในระบบไฮโดรโปนิกส์ (Hydroponics) และระบบซับสเตรท (Subtrate) ภายใต้ชื่อ "มิสเวจจี้" (Miss Veggie)
                            , จัดจำหน่ายผักพื้นบ้าน ผลไม้ ภายใต้ชื่อ "ซีบง" (C'EST BON),
                            ผลิตและจัดจำหน่ายถ่านไม้กะลาอัดแท่ง ถ่านไม้ยูคา ภายใต้ชื่อ "ซีฟาย" (C'FIRE) และบริการ
                            Supplier จัดซื้อจัดหาสินค้า ผักสลัด, ผักพื้นบ้าน, ผักไทย, ผลไม้, ถ่านกะลาอัดแท่ง, ถ่านไม้ยูคา
                        </p>
                        <p class="mb-4 FC_Ekaluck" style="text-indent: 2em;">
                            ฟาร์มผักสลัดไฮโดรโปนิกส์ และโรงผลิตถ่าน ตั้งอยุ่ที่ อำเภอด่านช้าง จัดหวัดสุพรรณบุรี มีศูนย์กระจายสินค้า อยู่ที่ พระราม3 เจริญราษฎร์7 กรุงเทพมหานคร
                        </p>
                        <div class="row g-0 mb-3">
                            <div class="col-sm-6 wow zoomIn" data-wow-delay="0.2s">
                                <h5 class="mb-3 FC_Ekaluck"><i class="fa fa-check text-success me-3"></i>บริการจัดส่ง
                                </h5>
                                <h5 class="mb-3 FC_Ekaluck"><i
                                        class="fa fa-check text-success me-3"></i>รับประกันสินค้า
                                </h5>
                            </div>
                            <div class="col-sm-6 wow zoomIn" data-wow-delay="0.4s">
                                <h5 class="mb-3 FC_Ekaluck"><i class="fa fa-check text-success me-3"></i>เคลมสินค้าได้
                                </h5>
                                <h5 class="mb-3 FC_Ekaluck"><i class="fa fa-check text-success me-3"></i>สะอาด ปลอดภัย ได้รับมาตรฐาน GAP
                                </h5>
                            </div>
                            <small style="color: red" class="FC_Ekaluck">
                                * ในการรับประกัน และ เคลมสินค้า จะต้องอยุ่ภายในระยะเวลาของวันที่รับสินค้า
                            </small>
                        </div>
                        <div class="d-flex align-items-center mb-4 wow fadeIn" data-wow-delay="0.6s">
                            <div class="bg-success d-flex align-items-center justify-content-center rounded"
                                style="width: 60px; height: 60px;">
                                <i class="fa fa-phone-alt text-white"></i>
                            </div>
                            <div class="ps-4">
                                <h5 class="mb-2 FC_Ekaluck">ติดต่อสอบถาม</h5>
                                <h4 class="text-success mb-0">062-2496424,062-4289551</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5" style="min-height: 500px;">
                        <div class="position-relative h-100">
                            <img width="100%" height="100%" class="position-absolute rounded wow zoomIn" data-wow-delay="0.9s"
                                src="img/company.jpg" style="object-fit: cover;" alt="บริษัท สิริ กรีน ฟาร์ม จำกัด, ฟาร์มผักสลัดไฮโดรโปนิกส์, โรงผลิตถ่านอัดแท่ง, เตาเผาถ่านไม้ยูคา">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid py-5 wow fadeInUp" data-wow-delay="0.1s" id="service">
            <div class="container py-5">
                <div class="section-title text-center position-relative pb-3 mb-5 mx-auto" style="max-width: 600px;">
                    <h5 class="fw-bold text-success text-uppercase FC_Ekaluck">บริการของเรา</h5>
                    <h1 class="mb-0 FC_Ekaluck">บริษัทของเราดำเนินกิจการเกี่ยวกับ ฟาร์มผักสลัดไฮโดรโปนิกส์, จัดจำหน่ายผักสลัดไฮโดรโปนิกส์,
                        จัดจำหน่ายผักไทย-ผักพื้นบ้าน-ผลไม้, ผลิตและจัดจำหน่ายถ่านกะลาอัดแท่ง-ถ่านไม้ยูคา</h1>
                </div>
                <div class="row g-5">
                    <div class="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.3s">
                        <div
                            class="service-item bg-light rounded d-flex flex-column align-items-center justify-content-center text-center">

                            <img width="150px" height="100px" src="img/farm.jpg" class="img-thumbnail mb-3"
                                alt="ฟาร์มผักสลัดด่านช้าง, ผักสลัดไฮโดรโปนิกส์, ผักสลัดไฮโดรโปนิกส์ พระราม3">

                            <h4 class="mb-3 FC_Ekaluck">ฟาร์มผักสลัดไฮโดรโปนิกส์</h4>
                            <p class="m-0 FC_Ekaluck">ได้รับมาตรฐาน GAP ผักสดจากฟาร์มอำเภอด่านช้าง จังหวัดสุพรรณบุรี</p>
                            <a class="btn btn-lg btn-success rounded" href="/farm">
                                <i class="bi bi-arrow-right"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.6s">
                        <div
                            class="service-item bg-light rounded d-flex flex-column align-items-center justify-content-center text-center">

                            <img width="150px" height="100px" src="img/missveggie1.jpg" style="width: 150px;" class="img-thumbnail mb-3"
                                alt="สลัดกล่อง, สลัดพร้อมทาน, MISSVEGGIE, ผักสลัดไฮโดรโปนิกส์">

                            <h4 class="mb-3 FC_Ekaluck">MISS VEGGIE</h4>
                            <p class="m-0 FC_Ekaluck">ดำเนินการขายผักสลัดแปรรูป เช่น สลัดกล่อง สลัดพร้อมทาน ผักสลัดไฮโดรโปนิกส์ และสินค้าอื่นๆ
                                ภายใต้แบรนด์(MISSVEGGIE)</p>
                            <a class="btn btn-lg btn-success rounded" href="/missveggie">
                                <i class="bi bi-arrow-right"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.9s">
                        <div
                            class="service-item bg-light rounded d-flex flex-column align-items-center justify-content-center text-center">

                            <img width="150px" height="100px" src="img/cbon.jpg" style="width: 150px;" class="img-thumbnail mb-3"
                                alt="จำหน่ายผักไทย ผักพื้นบ้าน ผลไม้ C'EST BON">

                            <h4 class="mb-3 FC_Ekaluck">C'EST BON</h4>
                            <p class="m-0 FC_Ekaluck">จำหน่ายผักไทย-ผักพื้นบ้าน-ผลไม้ ภายใต้แบรนด์(C'EST BON)</p>
                            <a class="btn btn-lg btn-success rounded" href="/missveggie">
                                <i class="bi bi-arrow-right"></i>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.6s">
                        <div
                            class="service-item bg-light rounded d-flex flex-column align-items-center justify-content-center text-center">

                            <img width="150px" height="100px" src="img/charcol.jpg" style="width: 150px;" class="img-thumbnail mb-3"
                                alt="จัดจำหน่าย ถ่านกะละอัดแท่ง ถ่านไม้ยูคา C'FIRE">

                            <h4 class="mb-3 FC_Ekaluck">C'FIRE ถ่านไม้ยูคา, ถ่านอัดแท่ง</h4>
                            <p class="m-0 FC_Ekaluck">ผลิตและจัดจำหน่ายถ่านกะละอัดแท่ง, ถ่านไม้ยูคา</p>
                            <a class="btn btn-lg btn-success rounded" href="/cfire">
                                <i class="bi bi-arrow-right"></i>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.3s">
                        <div
                            class="service-item bg-light rounded d-flex flex-column align-items-center justify-content-center text-center">
                            <img width="150px" height="100px" src="img/subplier.jpg" style="width: 150px;" class="img-thumbnail mb-3"
                                alt="บริการจัดหาผักสลัด ผักไทย ผลไม้">

                            <h4 class="mb-3 FC_Ekaluck">Supplier จัดซื้อจัดหาสินค้า</h4>
                            <p class="m-0 FC_Ekaluck">บริการจัดซื้อจัดหาผักสลัด, ผักไทย, ผลไม้, ถ่านไม้ยูคา, ถ่านอัดแท่ง
                                ตามที่ลูกค้าต้องการ ส่งถึงที่ราคาคาเป็นกันเอง</p>
                            <a class="btn btn-lg btn-success rounded" href="/supplier">
                                <i class="bi bi-arrow-right"></i>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.9s">
                        <div
                            class="position-relative bg-success rounded h-100 d-flex flex-column align-items-center justify-content-center text-center p-5">
                            <h3 class="text-white mb-3 FC_Ekaluck">มีข้อสงสัยติดต่อเรา</h3>
                            <p class="text-white mb-3 FC_Ekaluck">
                                ทางเรายินดีตอบคำถามทุกท่านที่สนใจในสินค้าและบริการของเรา</p>
                            <h2 class="text-white mb-0 FC_Ekaluck">062-249-6424</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="container-fluid py-5 wow fadeInUp" data-wow-delay="0.1s" id="offer">
            <div class="container py-5">
                <div class="row g-5">
                    <div class="col-lg-7">
                        <div class="section-title position-relative pb-3 mb-5">
                            <h5 class="fw-bold text-success text-uppercase FC_Ekaluck">ใบเสนอราคา</h5>
                            <h1 class="mb-0 FC_Ekaluck">สนใจสามารถกรอกข้อมูลเพื่อให้ทางเราติดต่อกลับ</h1>
                        </div>
                        <div class="row gx-3">
                            <div class="col-sm-6 wow zoomIn" data-wow-delay="0.2s">
                                <h5 class="mb-4 FC_Ekaluck"><i
                                        class="fa fa-reply text-success me-3"></i>เราจะติดต่อกลับภายใน 24 ชั่วโมง</h5>
                            </div>
                            <div class="col-sm-6 wow zoomIn" data-wow-delay="0.4s">
                                <h5 class="mb-4 FC_Ekaluck"><i class="fab fa-line text-success me-3"></i>0622496424
                                </h5>
                            </div>
                        </div>
                        <p class="mb-4 FC_Ekaluck">ต้องการใบเสนอราคากรุณาติดต่อเรา ทางเราจะทำการส่งใบเสนอราคาให้
                            ราคาที่เสนอให้อาจจะแตกต่างกันขึ้นอยู่กับ ราคาวัตถุดิบ ณ ช่วงเวลานั้น, ราคาน้ำมัน
                            (รายสัปดาห์, รายเดือน)</p>
                        <div class="d-flex align-items-center mt-2 wow zoomIn" data-wow-delay="0.6s">
                            <div class="bg-success d-flex align-items-center justify-content-center rounded"
                                style="width: 60px; height: 60px;">
                                <i class="fa fa-phone-alt text-white"></i>
                            </div>
                            <div class="ps-4">
                                <h5 class="mb-2 FC_Ekaluck">ติดต่อสอบถามได้ที่เบอร์</h5>
                                <h4 class="text-success mb-0 FC_Ekaluck">062 249 6424</h4>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-5">
                        <div class="bg-success rounded h-100 d-flex align-items-center p-5 wow zoomIn"
                            data-wow-delay="0.9s">

                                <from-order-price></from-order-price>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid py-5 wow fadeInUp" data-wow-delay="0.1s" id="deliver">
        <div class="container py-5">
            <div class="section-title text-center position-relative pb-3 mb-4 mx-auto" style="max-width: 600px;">
                <h5 class="fw-bold text-success text-uppercase FC_Ekaluck">พื่นที่ให้บริการ</h5>
                <h1 class="mb-0 FC_Ekaluck">บริการจัดส่งผักสลัด ผักไทย ผลไม้ ถ่านกะลาอัดแท่ง ถ่านไม้ยูคา</h1>
            </div>
            <div class="owl-carousel testimonial-carousel wow fadeInUp" data-wow-delay="0.6s">

                <div class="testimonial-item bg-light my-4">
                    <div class="d-flex align-items-center border-bottom pt-5 pb-4 px-5">
                        <div class="ps-1">
                            <h4 class="text-success mb-1 FC_Ekaluck">เขตบางคอแหลม</h4>
                            <small class="text-uppercase FC_Ekaluck">กรุงเทพมหานคร</small>
                        </div>
                    </div>
                    <div class="pt-4 pb-5 px-5">
                        <img width="100%" height="100%" src="img/zone/บางคอแหลม.jpg" class="img-thumbnail mb-3 center"
                            alt="บริการจัดส่งผักสลัด ผักไทย ผลไม้ ถ่านกะลาอัดแท่ง ถ่านไม้ยูคา เขตบางคอแหลม">
                    </div>
                </div>

                <div class="testimonial-item bg-light my-4">
                    <div class="d-flex align-items-center border-bottom pt-5 pb-4 px-5">
                        <div class="ps-1">
                            <h4 class="text-success mb-1 FC_Ekaluck">เขตธนบุรี</h4>
                            <small class="text-uppercase FC_Ekaluck">กรุงเทพมหานคร</small>
                        </div>
                    </div>
                    <div class="pt-4 pb-5 px-5">
                        <img width="100%" height="100%" src="img/zone/ธนบุรี.jpg" class="img-thumbnail mb-3 center"
                            alt="บริการจัดส่งผักสลัด ผักไทย ผลไม้ ถ่านกะลาอัดแท่ง ถ่านไม้ยูคา เขตธนบุรี">
                    </div>
                </div>

                <div class="testimonial-item bg-light my-4">
                    <div class="d-flex align-items-center border-bottom pt-5 pb-4 px-5">
                        <div class="ps-1">
                            <h4 class="text-success mb-1 FC_Ekaluck">เขตยานนาวา</h4>
                            <small class="text-uppercase FC_Ekaluck">กรุงเทพมหานคร</small>
                        </div>
                    </div>
                    <div class="pt-4 pb-5 px-5">
                        <img width="100%" height="100%" src="img/zone/ยานนาวา.jpg" class="img-thumbnail mb-3 center"
                            alt="บริการจัดส่งผักสลัด ผักไทย ผลไม้ ถ่านกะลาอัดแท่ง ถ่านไม้ยูคา เขตธนบุรี เขตยานนาวา">
                    </div>
                </div>

                <div class="testimonial-item bg-light my-4">
                    <div class="d-flex align-items-center border-bottom pt-5 pb-4 px-5">
                        <div class="ps-1">
                            <h4 class="text-success mb-1 FC_Ekaluck">เขตสาธร</h4>
                            <small class="text-uppercase FC_Ekaluck">กรุงเทพมหานคร</small>
                        </div>
                    </div>
                    <div class="pt-4 pb-5 px-5">
                        <img width="100%" height="100%" src="img/zone/สาธร.jpg" class="img-thumbnail mb-3 center"
                            alt="บริการจัดส่งผักสลัด ผักไทย ผลไม้ ถ่านกะลาอัดแท่ง ถ่านไม้ยูคา เขตธนบุรี เขตสาธร">
                    </div>
                </div>

                <div class="testimonial-item bg-light my-4">
                    <div class="d-flex align-items-center border-bottom pt-5 pb-4 px-5">
                        <div class="ps-1">
                            <h4 class="text-success mb-1 FC_Ekaluck">สุพรรณบุรี</h4>
                            <small class="text-uppercase FC_Ekaluck">อำเภอเมือง</small>
                        </div>
                    </div>
                    <div class="pt-4 pb-5 px-5">
                        <img width="100%" height="100%" src="img/zone/สุพรรณบุรี.jpg" class="img-thumbnail mb-3 center"
                            alt="บริการจัดส่งผักสลัด ผักไทย ผลไม้ ถ่านกะลาอัดแท่ง ถ่านไม้ยูคา เขตธนบุรี สุพรรณบุรี">
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- Testimonial End -->

</body>

<footer>
    @include('web_show.footer')

    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBKD9yhWTSmfID3mPBEhMmYFdBwzZkAlL8&callback=initMap&v=weekly" defer>
    </script>

    <script>
        // Initialize and add the map
        function initMap() {
            // The location of Uluru
            const uluru = {
                lat: 13.6937067,
                lng: 100.5156825
            };
            // The map, centered at Uluru
            const map = new google.maps.Map(document.getElementById("map"), {
                zoom: 12,
                center: uluru,
            });
            // The marker, positioned at Uluru
            const marker = new google.maps.Marker({
                position: uluru,
                map: map,
            });
        }

        window.initMap = initMap;
    </script>

    <!-- Back to Top -->
    <button href="#" class="btn btn-lg btn-success back-to-top" data-bs-toggle="modal" data-bs-target="#exampleModal"><span class="FC_Ekaluck">ใบเสนอราคา</span></button>

    <!-- JavaScript Libraries -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src={{ asset('lib/wow/wow.min.js') }}></script>
    <script src={{ asset('lib/easing/easing.min.js') }}></script>
    <script src={{ asset('lib/waypoints/waypoints.min.js') }}></script>
    <script src={{ asset('lib/counterup/counterup.min.js') }}></script>
    <script src={{ asset('lib/owlcarousel/owl.carousel.min.js') }}></script>

    <!-- Template Javascript -->
    <script src="js/main.js"></script>

    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script>
        $(document).ready(function() {
            if (getCookie('cookieConsent') !== 'true') {
                $('.cookie-consent').show();
            } else {
                $('.cookie-consent').hide();
            }

            $('.show-cookie-consent').click(function() {
                $('.cookie-consent').show();
            });

            $('.assent-cookie').click(function() {
                setCookie('cookieConsent', 'true', 365);
                $('.cookie-consent').hide();
            });

            $(window).scroll(function() {
                var bannerHeight = $('.cookie-consent').outerHeight();
                if ($(window).scrollTop() > bannerHeight) {
                    $('.cookie-consent').addClass('fixed');
                } else {
                    $('.cookie-consent').removeClass('fixed');
                }
            });
        });

        function getCookie(name) {
            var nameEQ = name + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
            }
            return null;
        }

        function setCookie(name, value, days) {
            var expires = "";
            if (days) {
                var date = new Date();
                date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                expires = "; expires=" + date.toUTCString();
            }
            document.cookie = name + "=" + (value || "") + expires + "; path=/";
        }
    </script>

</footer>

</html>

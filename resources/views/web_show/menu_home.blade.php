@extends('web_show.menu')

@section('content')
    <div class="container">

            <div class="collapse navbar-collapse" id="navbarCollapse">
                <div class="navbar-nav ms-auto py-0">
                    <a href="#home" class="nav-item nav-link font-menu">หน้าแรก</a>
                    <a href="#about" class="nav-item nav-link font-menu">เกี่ยวกับบริษัท</a>
                    <a href="#service" class="nav-item nav-link font-menu">บริการของเรา</a>
                    <div class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle color_nav_dropdown font-menu" data-bs-toggle="dropdown">สินค้าและบริการ</a>
                        <div class="dropdown-menu m-0">
                            <a href="{{route('home_farm')}}" class="dropdown-item font-menu">ผักสลัดไฮโดรโปนิกส์ </a>
                            <a href="{{route('home_missveggie')}}" class="dropdown-item font-menu">Miss Veggie</a>
                            <a href="{{route('home_supplier')}}" class="dropdown-item font-menu">จัดหาสินค้า (Supplier)</a>
                            <a href="{{route('home_cfire')}}" class="dropdown-item font-menu">ถ่านไม้กะลาอัดแท่ง & ถ่านไม้ยูคา</a>
                        </div>
                    </div>
                    <a href="#offer" class="nav-item nav-link font-menu">ใบเสนอราคา</a>
                    <a href="#deliver" class="nav-item nav-link font-menu">พื่นที่ให้บริการ</a>
                    <a href="#contact" class="nav-item nav-link font-menu">ติดต่อ</a>
                </div>
            </div>

    </div>
@endsection

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>ฟาร์มสลัดไฮโดรโปนิกส์ (สิริกรีน ฟาร์ม)</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="ผักสลัดไฮโดรโปนิกส์, ฟาร์มผักสลัดไฮโดรโปนิกส์, ผักสลัดด่านช้าง, ผักสลัดไฮโดรโปนิกส์พระราม3, ผักสลัดไฮโดรโปนิกส์ได้รับมาตรฐาน GAP, GAP, บริการจัดส่งผักสลัดไฮโดรโปนิกส์" name="keywords">
    <meta content="ผักสลัดไฮโดรโปนิกส์, ฟาร์มผักสลัดไฮโดรโปนิกส์" name="description">

    <!-- Favicon -->
    {{-- <link href="img/favicon.ico" rel="icon"> --}}
    <link rel="icon" href="{{asset("/img/logo_circle.png")}}">

    <!-- Google Web Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700;800&family=Rubik:wght@400;500;600;700&display=swap" rel="stylesheet">

    <!-- Icon Font Stylesheet -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">

    <!-- Libraries Stylesheet -->
    <link href={{asset("lib/owlcarousel/assets/owl.carousel.min.css")}} rel="stylesheet">
    <link href={{asset("lib/animate/animate.min.css")}} rel="stylesheet">

    <!-- Customized Bootstrap Stylesheet -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Template Stylesheet -->
    <link href="css/style.css" rel="stylesheet">

    <link href="css/home.css" rel="stylesheet">

    <style>
        body {
            font-family: FC_Ekaluck;
        }

        #map {
        height: 200px;
        /* The height is 400 pixels */
        width: 100%;
        /* The width is the width of the web page */
        }
    </style>

    <script src="{{ asset('js/app.js') }}" defer></script>

</head>

<body>
    {{-- <div id="app">
        <web-show-home></web-show-home>
    </div> --}}

    @include('web_show.menu_farm')
    @include('web_show.cookie_agree')

<div id="app">

            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                        <h5 class="modal-title FC_Ekaluck" id="exampleModalLabel">ใบเสนอราคา</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <from-order-price></from-order-price>
                        </div>
                        {{-- <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                        </div> --}}
                    </div>
                </div>
            </div>

    <div id="header-carousel" class="carousel slide carousel-fade" data-bs-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="w-100" src="img/farm/carousel_top-1.jpg" alt="ผักสลัดไฮโดรโปนิกส์, กรีนโอ๊ค, Green Oak, ฟาร์มผักสลัดไฮโดรโปนิกส์, ผักสลัดด่านช้าง, ผักสลัดไฮโดรโปนิกส์พระราม3, ผักสลัดไฮโดรโปนิกส์ได้รับมาตรฐาน GAP, GAP, บริการจัดส่งผักสลัดไฮโดรโปนิกส์">
                <div class="carousel-caption  d-flex flex-column align-items-center justify-content-center">
                    <div class="p-3 " style="max-width: 900px;">
                        <h3 class="text-white text-uppercase mb-3 animated slideInDown FC_Ekaluck">ฟาร์มผักสลัด</h3>
                        <h1 class="FC_Ekaluck display-1 text-white mb-md-4 animated zoomIn ">ผักสลัดไฮโดรโปนิกส์
                        </h1>
                        <button
                            class="btn btn-success btn-outline-light py-md-3 px-md-5 animated slideInRight FC_Ekaluck"
                            data-bs-toggle="modal" data-bs-target="#exampleModal"
                            style="background-color: #008400;"
                        >ใบเสนอราคา
                        </button>
                    </div>
                </div>


            </div>
            <div class="carousel-item ">
                <img class="w-100" src="img/farm/carousel_top-2.jpg" alt="ผักสลัดไฮโดรโปนิกส์, เรดโอ๊ค, Red Oak Lettuce, ฟาร์มผักสลัดไฮโดรโปนิกส์, ผักสลัดด่านช้าง, ผักสลัดไฮโดรโปนิกส์พระราม3, ผักสลัดไฮโดรโปนิกส์ได้รับมาตรฐาน GAP, GAP, บริการจัดส่งผักสลัดไฮโดรโปนิกส์">
                <div class="carousel-caption  d-flex flex-column align-items-center justify-content-center">
                    <div class="p-3 " style="max-width: 900px;">
                        <h3 class="text-white text-uppercase mb-3 animated slideInDown FC_Ekaluck">ฟาร์มผักสลัด</h3>
                        <h1 class="FC_Ekaluck display-1 text-white mb-md-4 animated zoomIn ">ผักสลัดไฮโดรโปนิกส์
                        </h1>
                        <button
                            class="btn btn-success btn-outline-light py-md-3 px-md-5 animated slideInRight FC_Ekaluck"
                            data-bs-toggle="modal" data-bs-target="#exampleModal"
                            style="background-color: #008400;"
                        >ใบเสนอราคา
                        </button>
                    </div>
                </div>
            </div>
            <div class="carousel-item ">
                <img class="w-100" src="img/farm/carousel_top-3.jpg" alt="ผักสลัดไฮโดรโปนิกส์, เรดคอรัล, Red Coral Lettuce, ฟาร์มผักสลัดไฮโดรโปนิกส์, ผักสลัดด่านช้าง, ผักสลัดไฮโดรโปนิกส์พระราม3, ผักสลัดไฮโดรโปนิกส์ได้รับมาตรฐาน GAP, GAP, บริการจัดส่งผักสลัดไฮโดรโปนิกส์">
                <div class="carousel-caption  d-flex flex-column align-items-center justify-content-center">
                    <div class="p-3 " style="max-width: 900px;">
                        <h3 class="text-white text-uppercase mb-3 animated slideInDown FC_Ekaluck">ฟาร์มผักสลัด</h3>
                        <h1 class="FC_Ekaluck display-1 text-white mb-md-4 animated zoomIn ">ผักสลัดไฮโดรโปนิกส์
                        </h1>
                        <button
                            class="btn btn-success btn-outline-light py-md-3 px-md-5 animated slideInRight FC_Ekaluck"
                            data-bs-toggle="modal" data-bs-target="#exampleModal"
                            style="background-color: #008400;"
                        >ใบเสนอราคา
                        </button>
                    </div>
                </div>
            </div>
            <div class="carousel-item ">
                <img class="w-100" src="img/farm/carousel_top-4.jpg" alt="ผักสลัดไฮโดรโปนิกส์, คอส, Cos Lettuce, ฟาร์มผักสลัดไฮโดรโปนิกส์, ผักสลัดด่านช้าง, ผักสลัดไฮโดรโปนิกส์พระราม3, ผักสลัดไฮโดรโปนิกส์ได้รับมาตรฐาน GAP, GAP, บริการจัดส่งผักสลัดไฮโดรโปนิกส์">
                <div class="carousel-caption  d-flex flex-column align-items-center justify-content-center">
                    <div class="p-3 " style="max-width: 900px;">
                        <h3 class="text-white text-uppercase mb-3 animated slideInDown FC_Ekaluck">ฟาร์มผักสลัด</h3>
                        <h1 class="FC_Ekaluck display-1 text-white mb-md-4 animated zoomIn ">ผักสลัดไฮโดรโปนิกส์
                        </h1>
                        <button
                            class="btn btn-success btn-outline-light py-md-3 px-md-5 animated slideInRight FC_Ekaluck"
                            data-bs-toggle="modal" data-bs-target="#exampleModal"
                            style="background-color: #008400;"
                        >ใบเสนอราคา
                        </button>
                    </div>
                </div>
            </div>
            <div class="carousel-item ">
                <img class="w-100" src="img/farm/carousel_top-5.jpg" alt="ผักสลัดไฮโดรโปนิกส์, ฟิลเลย์ไอซ์เบิร์ก, Frillice Ice Berg, ฟาร์มผักสลัดไฮโดรโปนิกส์, ผักสลัดด่านช้าง, ผักสลัดไฮโดรโปนิกส์พระราม3, ผักสลัดไฮโดรโปนิกส์ได้รับมาตรฐาน GAP, GAP, บริการจัดส่งผักสลัดไฮโดรโปนิกส์">
                <div class="carousel-caption  d-flex flex-column align-items-center justify-content-center">
                    <div class="p-3 " style="max-width: 900px;">
                        <h3 class="text-white text-uppercase mb-3 animated slideInDown FC_Ekaluck">ฟาร์มผักสลัด</h3>
                        <h1 class="FC_Ekaluck display-1 text-white mb-md-4 animated zoomIn ">ผักสลัดไฮโดรโปนิกส์
                        </h1>
                        <button
                            class="btn btn-success btn-outline-light py-md-3 px-md-5 animated slideInRight FC_Ekaluck"
                            data-bs-toggle="modal" data-bs-target="#exampleModal"
                            style="background-color: #008400;"
                        >ใบเสนอราคา
                        </button>
                    </div>
                </div>
            </div>
            <div class="carousel-item ">
                <img class="w-100" src="img/farm/carousel_top-6.jpg" alt="ผักสลัดไฮโดรโปนิกส์, บัตเตอร์เฮด, Green Butterhead, ฟาร์มผักสลัดไฮโดรโปนิกส์, ผักสลัดด่านช้าง, ผักสลัดไฮโดรโปนิกส์พระราม3, ผักสลัดไฮโดรโปนิกส์ได้รับมาตรฐาน GAP, GAP, บริการจัดส่งผักสลัดไฮโดรโปนิกส์">
                <div class="carousel-caption  d-flex flex-column align-items-center justify-content-center">
                    <div class="p-3 " style="max-width: 900px;">
                        <h3 class="text-white text-uppercase mb-3 animated slideInDown FC_Ekaluck">ฟาร์มผักสลัด</h3>
                        <h1 class="FC_Ekaluck display-1 text-white mb-md-4 animated zoomIn ">ผักสลัดไฮโดรโปนิกส์
                        </h1>
                        <button
                            class="btn btn-success btn-outline-light py-md-3 px-md-5 animated slideInRight FC_Ekaluck"
                            data-bs-toggle="modal" data-bs-target="#exampleModal"
                            style="background-color: #008400;"
                        >ใบเสนอราคา
                        </button>
                    </div>
                </div>
            </div>

        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#header-carousel"
            data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#header-carousel"
            data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>

    <!-- Full Screen Search Start -->
    <div class="modal fade" id="searchModal" tabindex="-1">
        <div class="modal-dialog modal-fullscreen">
            <div class="modal-content" style="background: rgba(9, 30, 62, .7);">
                <div class="modal-header border-0">
                    <button type="button" class="btn bg-white btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body d-flex align-items-center justify-content-center">
                    <div class="input-group" style="max-width: 600px;">
                        <input type="text" class="form-control bg-transparent border-primary p-3" placeholder="Type search keyword">
                        <button class="btn btn-success px-4"><i class="bi bi-search"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Full Screen Search End -->



    <!-- About Start -->
    <div class="container-fluid py-5 wow fadeInUp" data-wow-delay="0.1s" id="about">
        <div class="container py-5">
            <div class="row g-5">
                <div class="col-lg-7">
                    <div class="section-title position-relative pb-3 mb-5">
                        <h2 class="fw-bold text-success text-uppercase FC_Ekaluck">เกี่ยวกับฟาร์ม</h2>
                        <h1 class="mb-0 FC_Ekaluck">ฟาร์ม สิริกรีน</h1>
                    </div>
                    <p class="mb-4 FC_Ekaluck" style="text-indent: 2em;">ฟาร์มของเราปลูก ผักสลัดไฮโดรโปนิกส์ (Hydroponics) แบบซับสเตรท (Subtrate) โดยใช้ระบบ DFT (Deep Flow Technique) และ
                        NFT (Nutrient Film Technique) โดยผักสลัดที่ปลูกจะมี 8 ชนิด 1.กรีนโอ๊ค 2.บัตเตอร์เฮด 3.ฟิลเลย์ไอซ์เบิร์ก 4.เรดคอรัล 5.เรดโอ๊ค 6.คอส
                        7. ร็อกเก็ต 8. ไวลด์ ร็อกเก็ต
                        และผักพื้นบ้านต่างๆ
                    </p>
                    <p class="mb-4 FC_Ekaluck" style="text-indent: 2em;">
                        ผักสลัดของเราได้รับมาตรฐาน GAP มีบริการส่งถึงที่ในกรุงเทพโซนพระราม3 และบริเวณใกล้เคียง หรือมารับได้ที่ฟาร์มอำเภอด่านช้าง จังหวัดสุพรรณ
                    </p>
                    <div class="row g-0 mb-3">
                        <div class="col-sm-6 wow zoomIn" data-wow-delay="0.2s">
                            <h5 class="mb-3 FC_Ekaluck"><i class="fa fa-check text-success me-3"></i>จำหน่าย ปลีก-ส่ง</h5>
                            <h5 class="mb-3 FC_Ekaluck"><i class="fa fa-check text-success me-3"></i>มีบริการจัดส่งฟรี ในพื้นที่บริการ</h5>
                        </div>
                        <div class="col-sm-6 wow zoomIn" data-wow-delay="0.4s">
                            <h5 class="mb-3 FC_Ekaluck"><i class="fa fa-check text-success me-3"></i>เคลมสินค้าได้</h5>
                            <h5 class="mb-3 FC_Ekaluck"><i class="fa fa-check text-success me-3"></i>สะอาด ปลอดภัย ได้มาตรฐาน GAP</h5>
                        </div>
                        <small style="color: rgb(202, 13, 13)" class="FC_Ekaluck">* ในการรับประกัน และ เคลมสินค้า จะต้องอยุ่ภายในระยะเวลาของวันที่รับสินค้า</small>
                    </div>
                    <div class="d-flex align-items-center mb-4 wow fadeIn" data-wow-delay="0.6s">
                        <div class="bg-success d-flex align-items-center justify-content-center rounded" style="width: 60px; height: 60px;">
                            <i class="fa fa-phone-alt text-white"></i>
                        </div>
                        <div class="ps-4">
                            <h5 class="mb-2 FC_Ekaluck">ติดต่อสอบถาม</h5>
                            <h4 class="text-success mb-0">062-4289551 คุณเมย์, 062-2496424 คุณเตย</h4>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5" style="min-height: 500px;">
                    <div class="position-relative h-100">
                        <img class="position-absolute w-100 h-100 rounded wow zoomIn" data-wow-delay="0.9s" src="img/farm/farm.jpg" style="object-fit: cover;" alt="บริษัท สิริกรีน ฟาร์ม จำกัด, ฟาร์มผักสลัดไฮโดรโปนิกส์, ผักสลัดด่านช้าง, ผักสลัดไฮโดรโปนิกส์พระราม3, ผักสลัดไฮโดรโปนิกส์ได้รับมาตรฐาน GAP, GAP, บริการจัดส่งผักสลัดไฮโดรโปนิกส์">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- About End -->



                <!-- Service Start -->
        <div class="container-fluid py-5 wow fadeInUp" data-wow-delay="0.1s" id="product">
            <div class="container py-5">
                <div class="section-title text-center position-relative pb-3 mb-5 mx-auto" style="max-width: 600px;">
                    <h5 class="fw-bold text-success text-uppercase FC_Ekaluck">ผักสลัดไฮโดรโปนิกส์</h5>
                    <h1 class="mb-0 FC_Ekaluck">ชนิดของผักที่เราปลูก</h1>
                </div>

                <list-salad id_type="1"></list-salad>
            </div>
        </div>


        <div class="container-fluid py-5 wow fadeInUp" data-wow-delay="0.1s" id="offer">
            <div class="container py-5">
                <div class="row g-5">
                    <div class="col-lg-7">
                        <div class="section-title position-relative pb-3 mb-5">
                            <h5 class="fw-bold text-success text-uppercase FC_Ekaluck">ใบเสนอราคา</h5>
                            <h1 class="mb-0 FC_Ekaluck">สนใจสามารถกรอกข้อมูลเพื่อให้ทางเราติดต่อกลับ</h1>
                        </div>
                        <div class="row gx-3">
                            <div class="col-sm-6 wow zoomIn" data-wow-delay="0.2s">
                                <h5 class="mb-4 FC_Ekaluck"><i class="fa fa-reply text-success me-3"></i>เราจะติดต่อกลับภายใน 24 ชั่วโมง</h5>
                            </div>
                            <div class="col-sm-6 wow zoomIn" data-wow-delay="0.4s">
                                <h5 class="mb-4 FC_Ekaluck"><i class="fab fa-line text-success me-3"></i>0622496424</h5>
                            </div>
                        </div>
                        <p class="mb-4 FC_Ekaluck">ต้องการใบเสนอราคากรุณาติดต่อเรา ทางเราจะทำการส่งใบเสนอราคาให้ ราคาที่เสนอให้อาจจะแตกต่างกันขึ้นอยู่กับ ราคาวัตถุดิบ ณ ช่วงเวลานั้น, ราคาน้ำมัน (รายสัปดาห์, รายเดือน)</p>
                        <div class="d-flex align-items-center mt-2 wow zoomIn" data-wow-delay="0.6s">
                            <div class="bg-success d-flex align-items-center justify-content-center rounded" style="width: 60px; height: 60px;">
                                <i class="fa fa-phone-alt text-white"></i>
                            </div>
                            <div class="ps-4">
                                <h5 class="mb-2 FC_Ekaluck">ติดต่อสอบถามได้ที่เบอร์</h5>
                                <h4 class="text-success mb-0 FC_Ekaluck">062 249 6424</h4>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-5">
                        <div class="bg-success rounded h-100 d-flex align-items-center p-5 wow zoomIn" data-wow-delay="0.9s">

                                <from-order-price></from-order-price>

                                {{-- <router-link to="/user">Go to User</router-link>
                                <router-view></router-view> --}}

                        </div>
                    </div>

                </div>
            </div>
        </div>

</div>



    <div class="container-fluid py-5 wow fadeInUp" data-wow-delay="0.1s" id="deliver">
        <div class="container py-5">
            <div class="section-title text-center position-relative pb-3 mb-4 mx-auto" style="max-width: 600px;">
                <h5 class="fw-bold text-success text-uppercase FC_Ekaluck">พื่นที่ให้บริการ</h5>
                <h1 class="mb-0 FC_Ekaluck">บริการจัดส่งผักสลัด ผักไทย ผลไม้ ถ่านกะลาอัดแท่ง ถ่านไม้ยูคา</h1>
            </div>
            <div class="owl-carousel testimonial-carousel wow fadeInUp" data-wow-delay="0.6s">

                <div class="testimonial-item bg-light my-4">
                    <div class="d-flex align-items-center border-bottom pt-5 pb-4 px-5">
                        <div class="ps-1">
                            <h4 class="text-success mb-1 FC_Ekaluck">เขตบางคอแหลม</h4>
                            <small class="text-uppercase FC_Ekaluck">กรุงเทพมหานคร</small>
                        </div>
                    </div>
                    <div class="pt-4 pb-5 px-5">
                        <img src="img/zone/บางคอแหลม.jpg"  class="img-thumbnail mb-3 center" alt="บริการจัดส่งผักสลัด ผักไทย ผลไม้ ถ่านกะลาอัดแท่ง ถ่านไม้ยูคา เขตบางคอแหลม">
                    </div>
                </div>

                <div class="testimonial-item bg-light my-4">
                    <div class="d-flex align-items-center border-bottom pt-5 pb-4 px-5">
                        <div class="ps-1">
                            <h4 class="text-success mb-1 FC_Ekaluck">เขตธนบุรี</h4>
                            <small class="text-uppercase FC_Ekaluck">กรุงเทพมหานคร</small>
                        </div>
                    </div>
                    <div class="pt-4 pb-5 px-5">
                        <img src="img/zone/ธนบุรี.jpg"  class="img-thumbnail mb-3 center" alt="บริการจัดส่งผักสลัด ผักไทย ผลไม้ ถ่านกะลาอัดแท่ง ถ่านไม้ยูคา เขตธนบุรี">
                    </div>
                </div>

                <div class="testimonial-item bg-light my-4">
                    <div class="d-flex align-items-center border-bottom pt-5 pb-4 px-5">
                        <div class="ps-1">
                            <h4 class="text-success mb-1 FC_Ekaluck">เขตยานนาวา</h4>
                            <small class="text-uppercase FC_Ekaluck">กรุงเทพมหานคร</small>
                        </div>
                    </div>
                    <div class="pt-4 pb-5 px-5">
                        <img src="img/zone/ยานนาวา.jpg"  class="img-thumbnail mb-3 center" alt="บริการจัดส่งผักสลัด ผักไทย ผลไม้ ถ่านกะลาอัดแท่ง ถ่านไม้ยูคา เขตธนบุรี เขตยานนาวา">
                    </div>
                </div>

                <div class="testimonial-item bg-light my-4">
                    <div class="d-flex align-items-center border-bottom pt-5 pb-4 px-5">
                        <div class="ps-1">
                            <h4 class="text-success mb-1 FC_Ekaluck">เขตสาธร</h4>
                            <small class="text-uppercase FC_Ekaluck">กรุงเทพมหานคร</small>
                        </div>
                    </div>
                    <div class="pt-4 pb-5 px-5">
                        <img src="img/zone/สาธร.jpg"  class="img-thumbnail mb-3 center" alt="บริการจัดส่งผักสลัด ผักไทย ผลไม้ ถ่านกะลาอัดแท่ง ถ่านไม้ยูคา เขตธนบุรี เขตสาธร">
                    </div>
                </div>

                <div class="testimonial-item bg-light my-4">
                    <div class="d-flex align-items-center border-bottom pt-5 pb-4 px-5">
                        <div class="ps-1">
                            <h4 class="text-success mb-1 FC_Ekaluck">สุพรรณบุรี</h4>
                            <small class="text-uppercase FC_Ekaluck">อำเภอเมือง</small>
                        </div>
                    </div>
                    <div class="pt-4 pb-5 px-5">
                        <img src="img/zone/สุพรรณบุรี.jpg"  class="img-thumbnail mb-3 center" alt="บริการจัดส่งผักสลัด ผักไทย ผลไม้ ถ่านกะลาอัดแท่ง ถ่านไม้ยูคา เขตธนบุรี สุพรรณบุรี">
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- Testimonial End -->

    @include('web_show.footer')


    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBKD9yhWTSmfID3mPBEhMmYFdBwzZkAlL8&callback=initMap&v=weekly"
        defer
    ></script>

<script>
    // Initialize and add the map
    function initMap() {
    // The location of Uluru
    const uluru = { lat: 13.6937067, lng: 100.5156825 };
    // The map, centered at Uluru
    const map = new google.maps.Map(document.getElementById("map"), {
        zoom: 12,
        center: uluru,
    });
    // The marker, positioned at Uluru
    const marker = new google.maps.Marker({
        position: uluru,
        map: map,
    });
    }

    window.initMap = initMap;



</script>



    <!-- Back to Top -->
    {{-- <a href="#" class="btn btn-lg btn-success back-to-top"><i class="bi bi-arrow-up"></i></a> --}}
    <button href="#" class="btn btn-lg btn-success back-to-top" data-bs-toggle="modal" data-bs-target="#exampleModal"><span class="FC_Ekaluck">ใบเสนอราคา</span></button>




    <!-- JavaScript Libraries -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    {{-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script> --}}
    <script src={{asset("lib/wow/wow.min.js")}}></script>
    <script src={{asset("lib/easing/easing.min.js")}}></script>
    <script src={{asset("lib/waypoints/waypoints.min.js")}}></script>
    <script src={{asset("lib/counterup/counterup.min.js")}}></script>
    <script src={{asset("lib/owlcarousel/owl.carousel.min.js")}}></script>

    <!-- Template Javascript -->
    <script src="js/main.js"></script>

    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script>
        $(document).ready(function () {
            // $('html, body').animate({ scrollTop: $('#service').offset().top }, 'slow');
        })
    </script>
</body>

</html>

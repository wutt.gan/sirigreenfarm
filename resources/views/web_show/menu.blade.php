<style>
    .font-menu{
        font-size: 25px !important
    }
</style>


<!-- Spinner Start -->
    <div id="spinner" class="show bg-white position-fixed translate-middle w-100 vh-100 top-50 start-50 d-flex align-items-center justify-content-center">
        <div class="spinner"></div>
    </div>
    <!-- Spinner End -->

    <!-- Topbar Start -->
    <div class="container-fluid px-5 d-none d-lg-block" style="background-color: #087316" id="home">
        <div class="row gx-0">
            <div class="col-lg-8 text-center text-lg-start mb-2 mb-lg-0">
                <div class="d-inline-flex align-items-center" style="height: 45px;">
                    <small style="color:#ffffff" class="FC_Ekaluck">จัดจำหน่ายผักสลัดไฮโดรโปนิกส์ อาหารเพื่อสุขภาพ & จัดหาผักผลไม้ & จัดจำหน่าย ผลิตถ่านไม้กะลาอัดแท่ง</small>
                </div>
            </div>
            <div class="col-lg-4 text-center text-lg-end">
                <div class="d-inline-flex align-items-center" style="height: 45px;">
                    <a class="btn btn-sm btn-outline-light btn-sm-square rounded-circle me-2" href="https://www.facebook.com/sirigreenfarm46" target="_blank"
                        aria-label="facebook"
                    ><i class="fab fa-facebook-f fw-normal"></i></a>
                    <a aria-label="instagram" class="btn btn-sm btn-outline-light btn-sm-square rounded-circle me-2" href="#"><i class="fab fa-instagram fw-normal"></i></a>
                    <a aria-label="youtube" class="btn btn-sm btn-outline-light btn-sm-square rounded-circle" href="#"><i class="fab fa-youtube fw-normal"></i></a>
                </div>
            </div>
        </div>
    </div>
    <!-- Topbar End -->

    <!-- Navbar & Carousel Start -->
    <div class="container-fluid position-relative p-0">
        <nav class="navbar navbar-expand-xxl navbar-dark px-5 py-3 py-xxl-0">
            <div class="navbar-brand p-0">
                <a href="{{route('index')}}">
                    <img src="{{asset("/img/logo_circle.png")}}" style="margin-top: 1px;" alt="SIRIGREENFARM" width="75px" height="75px">
                    <h1 class="m-0 d-none d-xxl-block FC_Ekaluck"><span style="position: absolute;margin-top: -63px;margin-left: 90px;">SIRIGREENFARM</span></h1>
                    <h4 class="m-0 d-block d-none d-xxl-none d-md-block FC_Ekaluck"><span style="position: absolute;margin-top: -60px;margin-left: 90px;font-size: 35px;">SIRIGREENFARM</span></h4>
                </a>
            </div>
            <button id="navbar_buttton" aria-label="hamberger" class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse">
                <span class="fa fa-bars"></span>
            </button>

            @yield('content')
        </nav>
    </div>
    <!-- Navbar & Carousel End -->

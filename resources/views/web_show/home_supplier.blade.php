<!DOCTYPE html>
<html lang="en">

<!-- Meta Pixel Code -->
<script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window, document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '808494340360563');
    fbq('track', 'PageView');
</script>

    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=808494340360563&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Meta Pixel Code -->

<head>
    <meta charset="utf-8">
    <title>SUPPLIER BY SIRIGREENFARM (ผักสลัด ผักไทย ผลไม้ ถ่านกะลาอัดแท่ง ถ่านไม้ยูคา)</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="บริการจัดส่งผักผลไม้, บริการจัดส่งผักสลัด, พระราม3, กรุงเทพ, SUPPLIER, SIRIGREENFARM, ผักสลัด, ผักไทย, ผลไม้, ถ่านกะลาอัดแท่ง, ถ่านไม้ยูคา" name="keywords">
    <meta content="SUPPLIER SIRIGREENFARM บริการจัดส่งผักสลัด ผักผลไม้ ผักสลัด, ผักไทย, ผลไม้, ถ่านกะลาอัดแท่ง, ถ่านไม้ยูคา" name="description">

    <!-- Favicon -->
    {{-- <link href="img/favicon.ico" rel="icon"> --}}
    <link rel="icon" href="{{asset("/img/logo_circle.png")}}">

    <!-- Google Web Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700;800&family=Rubik:wght@400;500;600;700&display=swap" rel="stylesheet">

    <!-- Icon Font Stylesheet -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">

    <!-- Libraries Stylesheet -->
    <link href={{asset("lib/owlcarousel/assets/owl.carousel.min.css")}} rel="stylesheet">
    <link href={{asset("lib/animate/animate.min.css")}} rel="stylesheet">

    <!-- Customized Bootstrap Stylesheet -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Template Stylesheet -->
    <link href="css/style.css" rel="stylesheet">

    <link href="css/home.css" rel="stylesheet">

    <style>
        body {
            font-family: FC_Ekaluck;
        }
        .bg-primary {
            background-color: #008400 !important;
        }
        #map {
        height: 200px;
        /* The height is 400 pixels */
        width: 100%;
        /* The width is the width of the web page */
        }
        .text_red {
            color: #e61818
        }
        .section-title::before {
            position: absolute;
            content: "";
            width: 150px;
            height: 5px;
            left: 0;
            bottom: 0;
            background: #dc3545;
            border-radius: 2px;
        }
        .spinner {
            width: 40px;
            height: 40px;
            background: #dc3545;;
            margin: 100px auto;
            -webkit-animation: sk-rotateplane 1.2s infinite ease-in-out;
            animation: sk-rotateplane 1.2s infinite ease-in-out;
        }
        .testimonial-carousel .owl-dot.active {
            width: 30px;
            background: #dc3545;
        }
    </style>

    <script src="{{ asset('js/app.js') }}" defer></script>

</head>

<body>
    {{-- <div id="app">
        <web-show-home></web-show-home>
    </div> --}}

    @include('web_show.menu_supplier')
    @include('web_show.cookie_agree')

<div id="app">

            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                        <h5 class="modal-title FC_Ekaluck" id="exampleModalLabel">ใบเสนอราคา</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                                <from-order-price></from-order-price>
                        </div>
                        {{-- <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                        </div> --}}
                    </div>
                </div>
            </div>

    <div id="header-carousel" class="carousel slide carousel-fade" data-bs-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item">
                <img class="w-100" src="img/supplier/carousel_top-1.jpg" alt="supplier, บริการจัดส่งผักผลไม้, บริการจัดส่งผักสลัด, พระราม3, กรุงเทพ, SUPPLIER, ถ่านกะลาอัดแท่ง, ถ่านไม้ยูคา">

            </div>
            <div class="carousel-item active">
                <img class="w-100" src="img/supplier/carousel_top-2.jpg" alt="supplier, บริการจัดส่งผักผลไม้, บริการจัดส่งผักสลัด, พระราม3, กรุงเทพ, SUPPLIER, ถ่านกะลาอัดแท่ง, ถ่านไม้ยูคา">
                <div class="carousel-caption  d-flex flex-column align-items-center justify-content-center">
                    <div class="p-3 " style="max-width: 900px;">
                        <h3 class="text-white text-uppercase mb-3 animated slideInDown FC_Ekaluck">บริการซับพลายเออร์</h3>
                        <h1 class="FC_Ekaluck display-1 text-white mb-md-4 animated zoomIn ">ผักสลัด & ผักทั่วไป & ผลไม้
                        </h1>
                        <button
                            class="btn btn-success btn-outline-light py-md-3 px-md-5 animated slideInRight FC_Ekaluck"
                            data-bs-toggle="modal" data-bs-target="#exampleModal"
                            style="background-color: #008400;"
                        >ใบเสนอราคา
                        </button>
                    </div>
                </div>
            </div>



        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#header-carousel"
            data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#header-carousel"
            data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>




    <!-- About Start -->
    <div class="container-fluid py-5 wow fadeInUp" data-wow-delay="0.1s" id="about">
        <div class="container py-5">
            <div class="row g-5">
                <div class="col-lg-7">
                    <div class="section-title position-relative pb-3 mb-5">
                        <h2 class="fw-bold text_red text-uppercase FC_Ekaluck">เกี่ยวกับเรา</h2>
                        <h1 class="mb-0 FC_Ekaluck">บริการจัดหาสินค้าทางการเกษตร</h1>
                    </div>
                    <p class="mb-4 FC_Ekaluck" style="text-indent: 2em;">
                        บริการจัดซื้อ-จัดหาสินค้าทางการเกษตร ภายใต้ บริษัท สิริกรีน ฟาร์ม จำกัด โดยมีสินค้าต่างๆอาทิ เช่น
                        ผักสลัดไฮโดรโปนิกส์ ผัก-ผลไม้ต่างๆ ถ่านไม้กะลาอัดแท่ง ถ่านไม้ยูคา ส่งฟรีในเขตพระราม3 และบริเวณใกล้เคียง
                        พร้อมรับเคลมสินค้าที่มีปัญหาเมื่อสินค้าถึงมือลูกค้าภายในวันที่กำหนด
                    </p>
                    <p class="mb-4 FC_Ekaluck" style="text-indent: 2em;">
                        ผักสลัดของเราปลูกจากฟาร์มของเราเอง และได้รับมาตรฐาน GAP มั่นใจได้ว่าผักของเราสดใหม่แน่นอน
                    </p>
                    <div class="row g-0 mb-3">
                        <div class="col-sm-6 wow zoomIn" data-wow-delay="0.2s">
                            <h5 class="mb-3 FC_Ekaluck"><i class="fa fa-check text_red me-3"></i>บริการจัดส่ง</h5>
                            <h5 class="mb-3 FC_Ekaluck"><i class="fa fa-check text_red me-3"></i>รับประกันสินค้า</h5>
                        </div>
                        <div class="col-sm-6 wow zoomIn" data-wow-delay="0.4s">
                            <h5 class="mb-3 FC_Ekaluck"><i class="fa fa-check text_red me-3"></i>เคลมสินค้าได้</h5>
                            <h5 class="mb-3 FC_Ekaluck"><i class="fa fa-check text_red me-3"></i>สด ใหม่</h5>
                        </div>
                        <small style="color: rgb(202, 13, 13)" class="FC_Ekaluck">* ในการรับประกัน และ เคลมสินค้า จะต้องอยุ่ภายในระยะเวลาของวันที่รับสินค้า</small>
                    </div>
                    <div class="d-flex align-items-center mb-4 wow fadeIn" data-wow-delay="0.6s">
                        <div class="color_red d-flex align-items-center justify-content-center rounded" style="width: 60px; height: 60px;">
                            <i class="fa fa-phone-alt text-white"></i>
                        </div>
                        <div class="ps-4">
                            <h5 class="mb-2 FC_Ekaluck">ติดต่อสอบถาม</h5>
                            <h4 class="text_red mb-0">062-2496424,062-4289551</h4>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5" style="min-height: 500px;">
                    <div class="position-relative h-100">
                        <img class="position-absolute w-100 h-100 rounded wow zoomIn" data-wow-delay="0.9s" src="img/supplier/about.jpg" style="object-fit: cover;" alt="สินค้าเกษตร">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- About End -->

    <!-- Service Start -->
    <div class="container-fluid py-5 wow fadeInUp" data-wow-delay="0.1s" id="service">
        <div class="container py-5">
            <div class="section-title text-center position-relative pb-3 mb-5 mx-auto" style="max-width: 600px;">
                <h5 class="fw-bold text_red text-uppercase FC_Ekaluck">บริการของเรา</h5>
                <h1 class="mb-0 FC_Ekaluck">จัดหา-จัดซื้อ ผักสลัด ผัก-ผลไม้ ถ่านกะลาอัดแท่ง-ถ่านไม้ยูคา</h1>
            </div>
            <div class="row g-5">
                <div class="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.3s">
                    <div class="service-item bg-light rounded d-flex flex-column align-items-center justify-content-center text-center">

                        <img src="img/farm.jpg" style="width: 150px;" class="img-thumbnail mb-3" alt="ผักสลัดไฮโดรโปนิกส์, ฟาร์มผักสลัดไฮโดรโปนิกส์, ผักสลัดด่านช้าง, ผักสลัดไฮโดรโปนิกส์พระราม3, ผักสลัดไฮโดรโปนิกส์ได้รับมาตรฐาน GAP, GAP, บริการจัดส่งผักสลัดไฮโดรโปนิกส์">

                        <h4 class="mb-3 FC_Ekaluck">ผักสลัด</h4>
                        <p class="m-0 FC_Ekaluck">ผักสลัดไฮโดรโปนิกส์ ปลูกเองจากฟาร์ม สิริกรีน มาตรฐาน GAP</p>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.3s">
                    <div class="service-item bg-light rounded d-flex flex-column align-items-center justify-content-center text-center">
                        <img src="img/subplier.jpg" style="width: 150px;" class="img-thumbnail mb-3" alt="บริการจัดหาผักสลัด, ผักไทย, ผลไม้บริการจัดส่งผักผลไม้, บริการจัดส่งผักสลัด, พระราม3, กรุงเทพ">
                        {{-- <div class="service-icon">
                            <i class="fab fa-android text-white"></i>
                        </div> --}}
                        <h4 class="mb-3 FC_Ekaluck">ผัก-ผลไม้</h4>
                        <p class="m-0 FC_Ekaluck">ผัก-ผลไม้ สดใหม่ทุกวัน เพราะเราออกไปจ่ายตลาดให้ลูกค้าทุกวัน</p>

                    </div>
                </div>

                <div class="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.6s">
                    <div class="service-item bg-light rounded d-flex flex-column align-items-center justify-content-center text-center">

                        <img src="img/charcol.jpg" style="width: 150px;" class="img-thumbnail mb-3" alt="บริการจัดส่งถ่าน, จัดจำหน่าย, ถ่านกะละอัดแท่ง, ถ่านไม้ยูคา, C'FIRE, พระราม3, กรุงเทพ">

                        <h4 class="mb-3 FC_Ekaluck">ถ่านไม้ยูคา-ถ่านอัดแท่ง</h4>
                        <p class="m-0 FC_Ekaluck">ถ่านไม้เผาเอง จากฟาร์มสิริกรีน</p>

                    </div>
                </div>



                {{-- <div class="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.9s">
                    <div class="position-relative color_red rounded h-100 d-flex flex-column align-items-center justify-content-center text-center p-5">
                        <h3 class="text-white mb-3 FC_Ekaluck">มีข้อสงสัยติดต่อเรา</h3>
                        <p class="text-white mb-3 FC_Ekaluck">ทางเรายินดีตอบคำถามทุกท่านที่สนใจในสินค้าและบริการของเรา</p>
                        <h2 class="text-white mb-0 FC_Ekaluck">062-249-6424</h2>
                    </div>
                </div> --}}
            </div>
        </div>
    </div>


    <div class="container-fluid py-5 wow fadeInUp" data-wow-delay="0.1s" id="offer">
        <div class="container py-5">
            <div class="row g-5">
                <div class="col-lg-7">
                    <div class="section-title position-relative pb-3 mb-5">
                        <h5 class="fw-bold text_red text-uppercase FC_Ekaluck">ใบเสนอราคา</h5>
                        <h1 class="mb-0 FC_Ekaluck">สนใจสามารถกรอกข้อมูลเพื่อให้ทางเราติดต่อกลับ</h1>
                    </div>
                    <div class="row gx-3">
                        <div class="col-sm-6 wow zoomIn" data-wow-delay="0.2s">
                            <h5 class="mb-4 FC_Ekaluck"><i class="fa fa-reply text_red me-3"></i>เราจะติดต่อกลับภายใน 24 ชั่วโมง</h5>
                        </div>
                        <div class="col-sm-6 wow zoomIn" data-wow-delay="0.4s">
                            <h5 class="mb-4 FC_Ekaluck"><i class="fab fa-line text_red me-3"></i>0622496424</h5>
                        </div>
                    </div>
                    <p class="mb-4 FC_Ekaluck">ต้องการใบเสนอราคากรุณาติดต่อเรา ทางเราจะทำการส่งใบเสนอราคาให้ ราคาที่เสนอให้อาจจะแตกต่างกันขึ้นอยู่กับ ราคาวัตถุดิบ ณ ช่วงเวลานั้น, ราคาน้ำมัน (รายสัปดาห์, รายเดือน)</p>
                    <div class="d-flex align-items-center mt-2 wow zoomIn" data-wow-delay="0.6s">
                        <div class="color_red d-flex align-items-center justify-content-center rounded" style="width: 60px; height: 60px;">
                            <i class="fa fa-phone-alt text-white"></i>
                        </div>
                        <div class="ps-4">
                            <h5 class="mb-2 FC_Ekaluck">ติดต่อสอบถามได้ที่เบอร์</h5>
                            <h4 class="text_red mb-0 FC_Ekaluck">062 249 6424</h4>
                        </div>
                    </div>
                </div>

                <div class="col-lg-5">
                    <div class="color_red rounded h-100 d-flex align-items-center p-5 wow zoomIn" data-wow-delay="0.9s">
                        <div>
                            <from-order-price></from-order-price>

                            {{-- <router-link to="/user">Go to User</router-link>
                            <router-view></router-view> --}}
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>

    <div class="container-fluid py-5 wow fadeInUp" data-wow-delay="0.1s" id="deliver">
        <div class="container py-5">
            <div class="section-title text-center position-relative pb-3 mb-4 mx-auto" style="max-width: 600px;">
                <h5 class="fw-bold text_red text-uppercase FC_Ekaluck">พื่นที่ให้บริการ</h5>
                <h1 class="mb-0 FC_Ekaluck">บริการจัดส่งผักสลัด ผักไทย ผลไม้ ถ่านกะลาอัดแท่ง ถ่านไม้ยูคา</h1>
            </div>
            <div class="owl-carousel testimonial-carousel wow fadeInUp" data-wow-delay="0.6s">

                <div class="testimonial-item bg-light my-4">
                    <div class="d-flex align-items-center border-bottom pt-5 pb-4 px-5">
                        <div class="ps-1">
                            <h4 class="text_red mb-1 FC_Ekaluck">เขตบางคอแหลม</h4>
                            <small class="text-uppercase FC_Ekaluck">กรุงเทพมหานคร</small>
                        </div>
                    </div>
                    <div class="pt-4 pb-5 px-5">
                        <img src="img/zone/บางคอแหลม.jpg"  class="img-thumbnail mb-3 center" alt="บริการจัดส่งผักสลัด ผักไทย ผลไม้ ถ่านกะลาอัดแท่ง ถ่านไม้ยูคา เขตบางคอแหลม">
                    </div>
                </div>

                <div class="testimonial-item bg-light my-4">
                    <div class="d-flex align-items-center border-bottom pt-5 pb-4 px-5">
                        <div class="ps-1">
                            <h4 class="text_red mb-1 FC_Ekaluck">เขตธนบุรี</h4>
                            <small class="text-uppercase FC_Ekaluck">กรุงเทพมหานคร</small>
                        </div>
                    </div>
                    <div class="pt-4 pb-5 px-5">
                        <img src="img/zone/ธนบุรี.jpg"  class="img-thumbnail mb-3 center" alt="บริการจัดส่งผักสลัด ผักไทย ผลไม้ ถ่านกะลาอัดแท่ง ถ่านไม้ยูคา เขตธนบุรี">
                    </div>
                </div>

                <div class="testimonial-item bg-light my-4">
                    <div class="d-flex align-items-center border-bottom pt-5 pb-4 px-5">
                        <div class="ps-1">
                            <h4 class="text_red mb-1 FC_Ekaluck">เขตยานนาวา</h4>
                            <small class="text-uppercase FC_Ekaluck">กรุงเทพมหานคร</small>
                        </div>
                    </div>
                    <div class="pt-4 pb-5 px-5">
                        <img src="img/zone/ยานนาวา.jpg"  class="img-thumbnail mb-3 center" alt="บริการจัดส่งผักสลัด ผักไทย ผลไม้ ถ่านกะลาอัดแท่ง ถ่านไม้ยูคา เขตธนบุรี เขตยานนาวา">
                    </div>
                </div>

                <div class="testimonial-item bg-light my-4">
                    <div class="d-flex align-items-center border-bottom pt-5 pb-4 px-5">
                        <div class="ps-1">
                            <h4 class="text_red mb-1 FC_Ekaluck">เขตสาธร</h4>
                            <small class="text-uppercase FC_Ekaluck">กรุงเทพมหานคร</small>
                        </div>
                    </div>
                    <div class="pt-4 pb-5 px-5">
                        <img src="img/zone/สาธร.jpg"  class="img-thumbnail mb-3 center" alt="บริการจัดส่งผักสลัด ผักไทย ผลไม้ ถ่านกะลาอัดแท่ง ถ่านไม้ยูคา เขตธนบุรี เขตสาธร">
                    </div>
                </div>

                <div class="testimonial-item bg-light my-4">
                    <div class="d-flex align-items-center border-bottom pt-5 pb-4 px-5">
                        <div class="ps-1">
                            <h4 class="text_red mb-1 FC_Ekaluck">สุพรรณบุรี</h4>
                            <small class="text-uppercase FC_Ekaluck">อำเภอเมือง</small>
                        </div>
                    </div>
                    <div class="pt-4 pb-5 px-5">
                        <img src="img/zone/สุพรรณบุรี.jpg"  class="img-thumbnail mb-3 center" alt="บริการจัดส่งผักสลัด ผักไทย ผลไม้ ถ่านกะลาอัดแท่ง ถ่านไม้ยูคา เขตธนบุรี สุพรรณบุรี">
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- Testimonial End -->


    @include('web_show.footer_supplier')


    <script
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBKD9yhWTSmfID3mPBEhMmYFdBwzZkAlL8&callback=initMap&v=weekly"
      defer
    ></script>

<script>
    // Initialize and add the map
    function initMap() {
    // The location of Uluru
    const uluru = { lat: 13.6937067, lng: 100.5156825 };
    // The map, centered at Uluru
    const map = new google.maps.Map(document.getElementById("map"), {
        zoom: 12,
        center: uluru,
    });
    // The marker, positioned at Uluru
    const marker = new google.maps.Marker({
        position: uluru,
        map: map,
    });
    }

    window.initMap = initMap;



</script>


{{-- <button
class="btn btn-success btn-outline-light py-md-3 px-md-5 animated slideInRight FC_Ekaluck"
data-bs-toggle="modal" data-bs-target="#exampleModal"
style="background-color: #008400;"
>ใบเสนอราคา
</button> --}}


    <!-- Back to Top -->
    {{-- <a href="#" class="btn btn-lg btn-danger back-to-top"><i class="bi bi-arrow-up"></i></a> --}}
    <button href="#" class="btn btn-lg btn-danger back-to-top" data-bs-toggle="modal" data-bs-target="#exampleModal"><span class="FC_Ekaluck">ใบเสนอราคา</span></button>

    <!-- JavaScript Libraries -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    {{-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script> --}}
    <script src={{asset("lib/wow/wow.min.js")}}></script>
    <script src={{asset("lib/easing/easing.min.js")}}></script>
    <script src={{asset("lib/waypoints/waypoints.min.js")}}></script>
    <script src={{asset("lib/counterup/counterup.min.js")}}></script>
    <script src={{asset("lib/owlcarousel/owl.carousel.min.js")}}></script>

    <!-- Template Javascript -->
    <script src="js/main.js"></script>

    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script>

    </script>
</body>

</html>

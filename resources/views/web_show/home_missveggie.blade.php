<!DOCTYPE html>
<html lang="en">

<!-- Meta Pixel Code -->
{{-- <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window, document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '808494340360563');
    fbq('track', 'PageView');
</script>

    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=808494340360563&ev=PageView&noscript=1"
    /></noscript> --}}
    <!-- End Meta Pixel Code -->

<head>
    <meta charset="utf-8">
    <title>MISSVEGGIE (จำหน่ายผักสลัด สลัดพร้อมทาน สลัดกล่อง)</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="MISSVEGGIE, จำหน่ายผักสลัด, สลัดพร้อมทาน, สลัดกล่อง, ผักสลัดแปรรูปจากฟาร์ม, อาหารเพื่อสุขภาพ" name="keywords">
    <meta content="MISSVEGGIE, จำหน่ายผักสลัด, สลัดพร้อมทาน, สลัดกล่อง, ผักสลัดแปรรูปจากฟาร์ม, อาหารเพื่อสุขภาพ" name="description">

    <!-- Favicon -->
    {{-- <link href="img/favicon.ico" rel="icon"> --}}
    <link rel="icon" href="{{asset("/img/logo_circle.png")}}">

    <!-- Google Web Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700;800&family=Rubik:wght@400;500;600;700&display=swap" rel="stylesheet">

    <!-- Icon Font Stylesheet -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">

    <!-- Libraries Stylesheet -->
    <link href={{asset("lib/owlcarousel/assets/owl.carousel.min.css")}} rel="stylesheet">
    <link href={{asset("lib/animate/animate.min.css")}} rel="stylesheet">

    <!-- Customized Bootstrap Stylesheet -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Template Stylesheet -->
    <link href="css/style.css" rel="stylesheet">

    <link href="css/home.css" rel="stylesheet">

    <style>
        body {
            font-family: FC_Ekaluck;
        }

        #map {
        height: 200px;
        /* The height is 400 pixels */
        width: 100%;
        /* The width is the width of the web page */
        }
    </style>

    <script src="{{ asset('js/app.js') }}" defer></script>

</head>

<body>
    {{-- <div id="app">
        <web-show-home></web-show-home>
    </div> --}}

    @include('web_show.menu_missveggie')
    @include('web_show.cookie_agree')

    <div id="header-carousel" class="carousel slide carousel-fade" data-bs-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="w-100" src="img/missveggie/carousel_top-3.jpg" alt="missveggie, น้ำสลัด, น้ำสลัดทำเอง">
            </div>
            <div class="carousel-item ">
                <img class="w-100" src="img/missveggie/carousel_top-6.jpg" alt="missveggie, สลัดช่อ, สินค้าเพื่อสุขภาพ, ทำจากผักสลัดจากฟาร์ม">
            </div>
            <div class="carousel-item ">
                <img class="w-100" src="img/missveggie/carousel_top-2.jpg" alt="missveggie, สลัดกล่อง, สินค้าเพื่อสุขภาพ, ทำจากผักสลัดจากฟาร์ม">
            </div>
            <div class="carousel-item ">
                <img class="w-100" src="img/missveggie/carousel_top-1.jpg" alt="missveggie, น้ำสลัด, น้ำสลัดทำเอง">
            </div>

            <div class="carousel-item ">
                <img class="w-100" src="img/missveggie/carousel_top-4.jpg" alt="missveggie, สลัดโรล, สินค้าเพื่อสุขภาพ, ทำจากผักสลัดจากฟาร์ม">
            </div>
            <div class="carousel-item ">
                <img class="w-100" src="img/missveggie/carousel_top-5.jpg" alt="missveggie, แซนวิช, สินค้าเพื่อสุขภาพ, ทำจากผักสลัดจากฟาร์ม">
            </div>

        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#header-carousel"
            data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#header-carousel"
            data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>

    <!-- Full Screen Search Start -->
    <div class="modal fade" id="searchModal" tabindex="-1">
        <div class="modal-dialog modal-fullscreen">
            <div class="modal-content" style="background: rgba(9, 30, 62, .7);">
                <div class="modal-header border-0">
                    <button type="button" class="btn bg-white btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body d-flex align-items-center justify-content-center">
                    <div class="input-group" style="max-width: 600px;">
                        <input type="text" class="form-control bg-transparent border-primary p-3" placeholder="Type search keyword">
                        <button class="btn btn-success px-4"><i class="bi bi-search"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Full Screen Search End -->



    <!-- About Start -->
    <div class="container-fluid py-5 wow fadeInUp" data-wow-delay="0.1s" id="about">
        <div class="container py-5">
            <div class="row g-5">
                <div class="col-lg-7">
                    <div class="section-title position-relative pb-3 mb-5">
                        <h5 class="fw-bold text-success text-uppercase FC_Ekaluck">เกี่ยวกับ</h5>
                        <h1 class="mb-0 FC_Ekaluck">MISSVEGGIE</h1>
                    </div>
                    <p class="mb-4 FC_Ekaluck" style="text-indent: 2em;">
                        ผลิตสินค้าแปรรูปผักสลัด อาหารเพื่อสุขภาพวัตถุดิบสดใหม่จากฟาร์ม สิริกรีน เป็นผักที่ได้มาตรฐานGAP ผักสด สะอาด ปลอดภัย มีสินค้าให้เลือกมากมาย
                        อาทิ เช่น ผักสลัดช่อ ผักสลัดรวม ผักสลัดรวมพร้อมทาน ผักสลัดกล่อง น้ำสลัด สลัดโรล แซนวิชสลัด
                    </p>
                    <p class="mb-4 FC_Ekaluck" style="text-indent: 2em;">
                        สินค้าภายใต้แบรนด์ MISSVEGGIE เป็นสินค้าที่ทางเราปลูกเองจากฟาร์ม ทำเองโดยไม่ผ่านตัวแทนผลิต จึงมั้นใจได้ว่าสินค้านั้นสดใหม่ สะอาดเสมอ
                    </p>
                    <div class="section-title position-relative pb-3 mb-5">
                        <h1 class="mb-0 FC_Ekaluck">C'EST BON</h1>
                    </div>
                    <p class="mb-4 FC_Ekaluck">
                        ผัก-ผลไม้ ที่เลือกคัดสรรจากเรา ทำให้มีความ สดใหม่ทุกวัน มาอยู่ภายใต้แบรนด์ C'EST BON
                    </p>

                    <div class="row g-0 mb-3">
                        <div class="col-sm-6 wow zoomIn" data-wow-delay="0.2s">
                            <h5 class="mb-3 FC_Ekaluck"><i class="fa fa-check text-success me-3"></i>สด สะอาด</h5>
                            <h5 class="mb-3 FC_Ekaluck"><i class="fa fa-check text-success me-3"></i>สดใหม่ทุกว้น</h5>
                        </div>
                        <div class="col-sm-6 wow zoomIn" data-wow-delay="0.4s">
                            <h5 class="mb-3 FC_Ekaluck"><i class="fa fa-check text-success me-3"></i>ราคามิตรภาพ</h5>
                            {{-- <h5 class="mb-3 FC_Ekaluck"><i class="fa fa-check text-success me-3"></i>เคลมสินค้าได้</h5> --}}
                        </div>
                        {{-- <small style="color: red">* ในการรับประกัน และ เคลมสินค้า จะต้องอยุ่ภายในระยะเวลาของวันที่รับสินค้า</small> --}}
                    </div>
                    <div class="d-flex align-items-center mb-4 wow fadeIn" data-wow-delay="0.6s">
                        <div class="bg-success d-flex align-items-center justify-content-center rounded" style="width: 60px; height: 60px;">
                            <i class="fa fa-phone-alt text-white"></i>
                        </div>
                        <div class="ps-4">
                            <h5 class="mb-2 FC_Ekaluck">ติดต่อสอบถาม</h5>
                            <h4 class="text-success mb-0">062-2496424, 092-7409129</h4>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5" style="min-height: 500px;">
                    <div class="position-relative h-100">
                        <img class="position-absolute w-100 h-100 rounded wow zoomIn" data-wow-delay="0.9s" src="img/missveggie/about.jpg" style="object-fit: cover;" alt="missveggie">
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="container-fluid py-5 wow fadeInUp" data-wow-delay="0.1s" id="shop">
        <div class="container py-5">
            <div class="section-title text-center position-relative pb-3 mb-4 mx-auto" style="max-width: 600px;">
                <h5 class="fw-bold text-success text-uppercase FC_Ekaluck">หน้าร้านของเรา</h5>
                <h1 class="mb-0 FC_Ekaluck">MissVeggie Shop (Interchange 21)</h1>
            </div>

            <p class="mb-4 ps-5 pe-5 FC_Ekaluck">
                &nbsp;&nbsp;&nbsp;&nbsp;ร้าน Missveggie ของเราตั้งอยู่ภายในตึก interchange 21 เวลาเปิดทำการ 8.00-18.00 น. วันจันทร์-เสาร์
                ทางร้านจะขายของเกี่ยวกับ ผักผลไม้ สินค้าแปรรูปจากผักผลไม้ อาทิ เช่น ผักพื้นบ้าน ผักโครงการหลวง ผักปลอดสาร ผักสลัดกล่อง น้ำสลัด แซนวิชสลัด
                สลัดโรล น้ำส้ม เต้าหู้ และอื่นๆ ให้ลูกค้าได้เลือกซื้อ
            </p>

            <div class="owl-carousel testimonial-carousel wow fadeInUp" data-wow-delay="0.6s">

                <div class="testimonial-item bg-light my-4">
                    <div class="pt-4 pb-5 px-5">
                        <img src="img/missveggie/shop1.jpg"  class="img-thumbnail mb-3 center" alt="ร้าน missveggie">
                    </div>
                </div>
                <div class="testimonial-item bg-light my-4">
                    <div class="pt-4 pb-5 px-5">
                        <img src="img/missveggie/shop2.jpg"  class="img-thumbnail mb-3 center" alt="ร้าน missveggie">
                    </div>
                </div>
                <div class="testimonial-item bg-light my-4">
                    <div class="pt-4 pb-5 px-5">
                        <img src="img/missveggie/shop3.jpg"  class="img-thumbnail mb-3 center" alt="ร้าน missveggie">
                    </div>
                </div>
                <div class="testimonial-item bg-light my-4">
                    <div class="pt-4 pb-5 px-5">
                        <img src="img/missveggie/shop4.jpg"  class="img-thumbnail mb-3 center" alt="ร้าน missveggie">
                    </div>
                </div>
                <div class="testimonial-item bg-light my-4">
                    <div class="pt-4 pb-5 px-5">
                        <img src="img/missveggie/shop5.jpg"  class="img-thumbnail mb-3 center" alt="ร้าน missveggie">
                    </div>
                </div>



            </div>
        </div>
    </div>


    <!-- About End -->



    <div id="app">

            {{-- สินค้า --}}
            <div class="container-fluid py-5 wow fadeInUp" data-wow-delay="0.1s" id="product">
                <div class="container py-5">
                    <div class="section-title text-center position-relative pb-3 mb-4 mx-auto" style="max-width: 600px;">
                        <h1 class="mb-0 FC_Ekaluck">รายการสินค้า</h1>
                    </div>

                    {{-- missveggie --}}
                    <div class="text-center position-relative pb-3 mx-auto" style="max-width: 600px;">
                        <h5 class="fw-bold text-success text-uppercase FC_Ekaluck">สินค้าแปรรูป จากทางร้าน</h5>
                    </div>
                    <list-product-misssveggie id_type="5"></list-product-misssveggie>

                    <br><hr>

                    {{-- ผักผลไม้ --}}
                    <div class="text-center position-relative pb-3 mx-auto" style="max-width: 600px;">
                        <h5 class="fw-bold text-success text-uppercase FC_Ekaluck">ผัก-ผลไม้</h5>
                    </div>
                    <list-product-misssveggie id_type="2"></list-product-misssveggie>

                    <br><hr>
                    {{-- ผักผลไม้ --}}
                    <div class="text-center position-relative pb-3 mx-auto" style="max-width: 600px;">
                        <h5 class="fw-bold text-success text-uppercase FC_Ekaluck">สินค้าแปรรูปอื่นๆ</h5>
                    </div>
                    <list-product-misssveggie id_type="6"></list-product-misssveggie>



                </div>
            </div>



    </div>

<footer>
    <!-- Testimonial End -->

    @include('web_show.footer_missveggie')


    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBKD9yhWTSmfID3mPBEhMmYFdBwzZkAlL8&callback=initMap&v=weekly"
        defer
    ></script>

    <script>
    // Initialize and add the map
    function initMap() {
    // The location of Uluru
    const uluru = { lat: 13.7366327, lng: 100.5627379 };
    // The map, centered at Uluru
    const map = new google.maps.Map(document.getElementById("map"), {
        zoom: 12,
        center: uluru,
    });
    // The marker, positioned at Uluru
    const marker = new google.maps.Marker({
        position: uluru,
        map: map,
    });
    }

    window.initMap = initMap;



    </script>

</footer>






    <!-- Back to Top -->
    <a href="#" aria-label="up-botton" class="btn btn-lg btn-success back-to-top"><i class="bi bi-arrow-up"></i></a>




    <!-- JavaScript Libraries -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    {{-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script> --}}
    <script src={{asset("lib/wow/wow.min.js")}}></script>
    <script src={{asset("lib/easing/easing.min.js")}}></script>
    <script src={{asset("lib/waypoints/waypoints.min.js")}}></script>
    <script src={{asset("lib/counterup/counterup.min.js")}}></script>
    <script src={{asset("lib/owlcarousel/owl.carousel.min.js")}}></script>

    <!-- Template Javascript -->
    <script src="js/main.js"></script>

    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script>
        $(document).ready(function () {
            // $('html, body').animate({ scrollTop: $('#service').offset().top }, 'slow');
        })
    </script>
</body>

</html>


    @extends('web_show.menu')

    @section('content')
        <div class="container">

            <div class="collapse navbar-collapse" id="navbarCollapse">
                <div class="navbar-nav ms-auto py-0 font-menu">
                    <a href="#home" class="nav-item nav-link">หน้าแรก</a>
                    <a href="#about" class="nav-item nav-link">เกี่ยวกับเรา</a>
                    <a href="#product" class="nav-item nav-link">สินค้าที่ให้บริการ</a>
                    <div class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle color_nav_dropdown" data-bs-toggle="dropdown">สินค้าและบริการอื่นๆ</a>
                        <div class="dropdown-menu m-0">
                            <a href="/" class="dropdown-item font-menu">Sirigreenfarm</a>
                            <a href="{{route('home_farm')}}" class="dropdown-item font-menu">ผักสลัดไฮโดรโปนิกส์ </a>
                            <a href="/missveggie" class="dropdown-item font-menu">Miss Veggie</a>
                            <a href="{{route('home_cfire')}}" class="dropdown-item font-menu">ถ่านไม้กะลาอัดแท่ง & ถ่านไม้ยูคา</a>
                        </div>
                    </div>
                    <a href="#offer" class="nav-item nav-link">ใบเสนอราคา</a>

                    <a href="#deliver" class="nav-item nav-link">พื่นที่ให้บริการ</a>
                    <a href="#contact" class="nav-item nav-link ">ติดต่อ</a>
                </div>
            </div>


        </div>
    @endsection



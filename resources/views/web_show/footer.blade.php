    <!-- Footer Start -->
    <div class="container-fluid bg-dark text-light mt-5 wow fadeInUp" data-wow-delay="0.1s" id="contact">
        <div class="container">
            <div class="row gx-5">
                <div class="col-lg-4 col-md-6 footer-about">
                    <div class="d-flex flex-column align-items-center justify-content-center text-center h-100 bg-success p-4">
                        <a href="#" class="navbar-brand">
                            <img width="65px" height="65px" src="{{asset("/img/logo_circle.png")}}" style="margin-top: 1px;" alt="SIRIGREENFARM">
                            <br>
                            <h3 class="m-0 text-white FC_Ekaluck">SIRIGREENFARM</h3>
                        </a>
                        <p class="mt-3 mb-4 FC_Ekaluck">จัดจำหน่าย ฟาร์มผักสลัดไฮโดรโปนิกส์ เพื่อสุขภาพ & จัดหาผักผลไม้ & จัดจำหน่าย ผลิตถ่านไม้กะลาอัดแท่ง</p>
                    </div>
                </div>
                <div class="col-lg-8 col-md-6">
                    <div class="row gx-5">
                        <div class="col-lg-6 col-md-12 pt-5 mb-5">
                            <div class="section-title section-title-sm position-relative pb-3 mb-4">
                                <h3 class="text-light mb-0 FC_Ekaluck">ติดต่อเรา</h3>
                            </div>
                            <div class="d-flex mb-2">
                                <i class="bi bi-geo-alt text-success me-2"></i>
                                <p class="mb-0 FC_Ekaluck">129 เจริญราษฎร์ 7 แยก 11 แขวงบางโคล่ เขตบางคอแหลม กทม. 10120</p>
                            </div>
                            <div class="d-flex mb-2">
                                <i class="bi bi-envelope-open text-success me-2"></i>
                                <p class="mb-0 FC_Ekaluck">Email: sirigreenfarm4656@gmail.com</p>
                            </div>
                            <div class="d-flex mb-2">
                                <i class="bi bi-telephone text-success me-2"></i>
                                <p class="mb-0 FC_Ekaluck">Tel: 062-249-6424</p>
                            </div>
                            <div class="d-flex mb-2">
                                <i class="fab fa-line text-success me-2"></i>
                                <p class="mb-0 FC_Ekaluck">Line Id: 0622496424</p>
                            </div>
                            <div class="d-flex mt-4">
                                {{-- <a class="btn btn-success btn-square me-2" href="#"><i class="fab fa-twitter fw-normal"></i></a> --}}
                                <a class="btn btn-success btn-square me-2" href="https://www.facebook.com/sirigreenfarm46" target="_blank"><i class="fab fa-facebook-f fw-normal"></i></a>
                                <a class="btn btn-success btn-square me-2" href="#"><i class="fab fa-linkedin-in fw-normal"></i></a>
                                <a class="btn btn-success btn-square" href="#"><i class="fab fa-instagram fw-normal"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12 pt-0 pt-lg-5 mb-5">
                            <div class="section-title section-title-sm position-relative pb-3 mb-4">
                                <h3 class="text-light mb-0 FC_Ekaluck">ที่ตั้งบริษัท</h3>
                            </div>
                            <div class="link-animated d-flex flex-column justify-content-start">
                                {{-- <div id="map"></div> --}}
                                <a target="_blank" href="https://www.google.com/maps/place/%E0%B8%9C%E0%B8%B1%E0%B8%81%E0%B8%AA%E0%B8%A5%E0%B8%B1%E0%B8%94%E0%B8%88%E0%B8%B2%E0%B8%81%E0%B8%9F%E0%B8%B2%E0%B8%A3%E0%B9%8C%E0%B8%A1+%E0%B8%9C%E0%B8%B1%E0%B8%81%E0%B9%84%E0%B8%AE%E0%B9%82%E0%B8%94%E0%B8%A3%E0%B9%82%E0%B8%9B%E0%B8%99%E0%B8%B4%E0%B8%81%E0%B8%AA%E0%B9%8C+%E0%B8%9E%E0%B8%A3%E0%B8%B0%E0%B8%A3%E0%B8%B2%E0%B8%A13+(%E0%B8%9A%E0%B8%A3%E0%B8%B4%E0%B8%A9%E0%B8%B1%E0%B8%97+%E0%B8%AA%E0%B8%B4%E0%B8%A3%E0%B8%B4+%E0%B8%81%E0%B8%A3%E0%B8%B5%E0%B8%99+%E0%B8%9F%E0%B8%B2%E0%B8%A3%E0%B9%8C%E0%B8%A1+%E0%B8%88%E0%B8%B3%E0%B8%81%E0%B8%B1%E0%B8%94)/@13.6938712,100.4725876,11z/data=!4m6!3m5!1s0x30e2994a2fdba9c3:0x77bed54e61d418b3!8m2!3d13.6937565!4d100.5156854!16s%2Fg%2F11kj41d6k1?entry=ttu">
                                    <img width="100%" height="100%" src="img/map_sirigreen.png" class="img-thumbnail mb-3 center"
                                        alt="ผักสลัดจากฟาร์ม ผักไฮโดรโปนิกส์ พระราม3 (บริษัท สิริ กรีน ฟาร์ม จำกัด)" >
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid text-white" style="background: #065310;">
        <div class="container text-center">
            <div class="row justify-content-end">
                <div class="col-lg-8 col-md-6">
                    <div class="d-flex align-items-center justify-content-center" style="height: 75px;">
                        <p class="mb-0"> All Rights Reserved.

						<!--/*** This template is free as long as you keep the footer author’s credit link/attribution link/backlink. If you'd like to use the template without the footer author’s credit link/attribution link/backlink, you can purchase the Credit Removal License from "https://htmlcodex.com/credit-removal". Thank you for your support. ***/-->
						Designed by <a class="text-white border-bottom" href="https://htmlcodex.com">HTML Codex</a>

                        Distributed By: <a class="text-white border-bottom" href="https://themewagon.com" target="_blank">ThemeWagon</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer End -->

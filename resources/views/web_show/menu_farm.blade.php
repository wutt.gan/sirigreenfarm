


@extends('web_show.menu')

@section('content')
    <div class="container">

        <div class="collapse navbar-collapse" id="navbarCollapse">
            <div class="navbar-nav ms-auto py-0 font-menu">
                <a href="#home" class="nav-item nav-link">หน้าแรก</a>
                <a href="#about" class="nav-item nav-link">เกี่ยวกับฟาร์ม</a>
                <a href="#product" class="nav-item nav-link">ชนิดผัก</a>
                <div class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle color_nav_dropdown" data-bs-toggle="dropdown">สินค้าและบริการอื่นๆ</a>
                    <div class="dropdown-menu m-0">
                        <a href="/" class="dropdown-item font-menu">Sirigreenfarm</a>
                        <a href="/missveggie" class="dropdown-item font-menu">Miss Veggie</a>
                        <a href="{{route('home_supplier')}}" class="dropdown-item font-menu">จัดหาสินค้า (Supplier)</a>
                        <a href="{{route('home_cfire')}}" class="dropdown-item font-menu">ถ่านไม้กะลาอัดแท่ง & ถ่านไม้ยูคา</a>
                    </div>
                </div>
                <a href="#offer" class="nav-item nav-link">ใบเสนอราคา</a>

                <a href="#deliver" class="nav-item nav-link">พื่นที่ให้บริการ</a>
                <a href="#contact" class="nav-item nav-link ">ติดต่อ</a>
            </div>
            {{-- <button type="button" class="btn text-primary ms-3" data-bs-toggle="modal" data-bs-target="#searchModal"><i class="fa fa-search"></i></button> --}}
            {{-- <a href="https://htmlcodex.com/startup-company-website-template" class="btn btn-primary py-2 px-4 ms-3">เข้าสู่ระบบ</a> --}}
        </div>



    </div>
@endsection

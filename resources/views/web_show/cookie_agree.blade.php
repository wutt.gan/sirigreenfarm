<div class="container mt-3 fixed-bottom">
    <div class="row">
        <div class="col-12">
            <div class="row alert alert-warning alert-dismissible fade show cookie-consent display-none"
                role="alert">
                <div class="col-11">
                    We use cookies to ensure that we give you the best experience on our website. By continuing to use
                    this site, you agree to our use of cookies.

                </div>
                <div class="col-1 text-right" style="text-align: right;">
                    <button type="button" class="btn btn-primary btn-sm ml-2 assent-cookie" data-dismiss="alert"
                        aria-label="Close">
                        Got it
                    </button>

                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>

<script>
    $(document).ready(function() {
        if (getCookie('cookieConsent') !== 'true') {
            $('.cookie-consent').show();
        } else {
            $('.cookie-consent').hide();
        }

        $('.show-cookie-consent').click(function() {
            $('.cookie-consent').show();
        });

        $('.assent-cookie').click(function() {
            setCookie('cookieConsent', 'true', 365);
            $('.cookie-consent').hide();
        });

        $(window).scroll(function() {
            var bannerHeight = $('.cookie-consent').outerHeight();
            if ($(window).scrollTop() > bannerHeight) {
                $('.cookie-consent').addClass('fixed');
            } else {
                $('.cookie-consent').removeClass('fixed');
            }
        });
    });

    function getCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
        }
        return null;
    }

    function setCookie(name, value, days) {
        var expires = "";
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = "; expires=" + date.toUTCString();
        }
        document.cookie = name + "=" + (value || "") + expires + "; path=/";
    }
</script>

@extends('layouts.app')

<style>

</style>

@section('content')
<div class="">

    <div class="row align-items-center h-100">
        <div class="col-md-7">
            <img class="img-fluid h-100" src="img/farm/farm.jpg" alt="ผักสลัดไฮโดรโปนิกส์ สลัดกล่องพร้อมทาน">

        </div>
        <div class="col-md-5 p-5 align-items-center">

            <div class="card shadow-lg ">
                <div class="card-body p-5">
                    <h1 class="fs-4 card-title fw-bold mb-4">Login</h1>
                    <form method="POST" class="needs-validation" novalidate="" action="{{ route('login') }}" autocomplete="off">
                        @csrf
                        <div class="mb-3">



                            <label class="mb-2 text-muted" for="email">E-Mail Address</label>
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="mb-3">
                            <div class="mb-2 w-100">
                                <label class="text-muted" for="password">Password</label>
                                {{-- <a href="forgot.html" class="float-end">
                                    Forgot Password?
                                </a> --}}
                            </div>
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror

                            <div class="invalid-feedback">
                                Password is required
                            </div>
                        </div>



                        <div class="d-flex align-items-center">
                            {{-- <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                <label for="remember" class="form-check-label">Remember Me</label>
                            </div> --}}
                            <button type="submit" class="btn btn-primary ms-auto">
                                Login
                            </button>
                        </div>

                        {{-- @if (Route::has('password.request'))
                            <a class="btn btn-link" href="{{ route('password.request') }}">
                                {{ __('Forgot Your Password?') }}
                            </a>
                        @endif --}}



                    </form>
                </div>
                {{-- <div class="card-footer py-3 border-0">
                    <div class="text-center">
                        Don't have an account? <a href="register.html" class="text-dark">Create One</a>
                    </div>
                </div> --}}
            </div>

        </div>
    </div>


</div>
@endsection

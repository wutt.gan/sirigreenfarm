<!DOCTYPE html>

<!-- =========================================================
* Sneat - Bootstrap 5 HTML Admin Template - Pro | v1.0.0
==============================================================

* Product Page: https://themeselection.com/products/sneat-bootstrap-html-admin-template/
* Created by: ThemeSelection
* License: You must have a valid license purchased in order to legally use the theme for your project.
* Copyright ThemeSelection (https://themeselection.com)

=========================================================
 -->
<!-- beautify ignore:start -->
<html
  lang="en"
  class="light-style layout-menu-fixed"
  dir="ltr"
  data-theme="theme-default"
  data-assets-path="{{ asset('assets/')}}"
  data-template="vertical-menu-template-free"
>
  <head>
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0"
    />

    <title>(HR)SIRIGREENFARM</title>

    <meta name="description" content="" />



    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('assets/vendor/js/menu.js') }}"></script>

    <!-- Favicon -->
    <link rel="icon" href="{{ asset('/img/logo_circle.png') }}">


    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link
      href="https://fonts.googleapis.com/css2?family=Public+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap"
      rel="stylesheet"
    />

    <!-- Icons. Uncomment required icon fonts -->
    <link rel="stylesheet" href="{{ asset('assets/vendor/fonts/boxicons.css') }}" />



    <!-- Core CSS -->
    <link rel="stylesheet" href="{{ asset('assets/vendor/css/core.css') }}" class="template-customizer-core-css" />
    <link rel="stylesheet" href="{{ asset('assets/vendor/css/theme-default.css') }}" class="template-customizer-theme-css" />
    <link rel="stylesheet" href="{{ asset('assets/css/demo.css') }}" />

    <!-- Vendors CSS -->
    <link rel="stylesheet" href="{{ asset('assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.css') }}" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <link href="{{ asset('css/admin/datatable.css') }}" rel="stylesheet">



    <!-- Page CSS -->

    <!-- Helpers -->
    <script src="{{ asset('assets/vendor/js/helpers.js') }}"></script>

    <!--! Template customizer & Theme config files MUST be included after core stylesheets and helpers.js in the <head> section -->
    <!--? Config:  Mandatory theme config file contain global vars & default theme options, Set your preferred theme option in this file.  -->
    <script src="{{ asset('assets/js/config.js') }}"></script>

    <!-- Scripts -->

  </head>

  <body>
    <!-- Layout wrapper -->
    <div id="">
        <div class="layout-wrapper layout-content-navbar">
          <div class="layout-container">
            <!-- Menu -->


            {{-- <aside id="layout-menu" class="layout-menu menu-vertical menu bg-menu-theme">
              <div class="app-brand demo">
                <a href="#" class="app-brand-link">
                  <span class="app-brand-logo demo">

                    <img src="{{asset("/img/logo_circle.png")}}" alt="" style="width: 35px;">

                  </span>
                  <span class="app-brand-text fw-bolder ms-2">SirigreenFarm11</span>
                </a>

                <a href="javascript:void(0);" class="layout-menu-toggle menu-link text-large ms-auto d-block d-xl-none">
                  <i class="bx bx-chevron-left bx-sm align-middle"></i>
                </a>
              </div>

              <div class="menu-inner-shadow"></div>

              <ul class="menu-inner py-1">
                <!-- Dashboard -->
                <li class="menu-item {{ request()->is('home') ? 'active' : '' }}">
                  <a href="{{route('home')}}" class="menu-link">
                    <i class="menu-icon tf-icons bx bx-home-circle"></i>
                    <div data-i18n="Analytics">Dashboard</div>
                  </a>
                </li>

                <li class="menu-item {{ request()->is('formorder/*') ? 'active' : '' }}">
                    <a href="{{route('formorder.list')}}" class="menu-link">
                        <i class="menu-icon tf-icons bx bx-crown"></i>
                        <div data-i18n="Boxicons">คำร้องใบเสนอราคา</div>
                    </a>
                </li>


                <li class="menu-header small text-uppercase">
                    <i class="fa-solid fa-bag-shopping"></i>
                  <span class="menu-header-text">Product</span>
                </li>
                <li class="menu-item">
                  <a href="javascript:void(0);" class="menu-link menu-toggle">
                    <i class="menu-icon tf-icons bx bx-dock-top"></i>
                    <div data-i18n="Account Settings">สินค้าหน้าเว็ป123</div>
                  </a>
                  <ul class="menu-sub">
                    <li class="menu-item">
                      <a href="pages-account-settings-account.html" class="menu-link">
                        <div data-i18n="1">ผักสลัด</div>
                      </a>
                    </li>
                    <li class="menu-item">
                      <a href="pages-account-settings-notifications.html" class="menu-link">
                        <div data-i18n="2">ผักไทย</div>
                      </a>
                    </li>
                    <li class="menu-item">
                      <a href="pages-account-settings-notifications.html" class="menu-link">
                        <div data-i18n="3">ผลไม้</div>
                      </a>
                    </li>
                    <li class="menu-item">
                      <a href="pages-account-settings-connections.html" class="menu-link">
                        <div data-i18n="4">ถ่าน</div>
                      </a>
                    </li>
                  </ul>
                </li>

                <li class="menu-item">
                  <a href="icons-boxicons.html" class="menu-link">
                    <i class="menu-icon tf-icons bx bx-crown"></i>
                    <div data-i18n="Boxicons">Boxicons</div>
                  </a>
                </li>

                <li class="menu-header small text-uppercase">
                    <i class="fa-solid fa-gear me-2"></i>
                    <span class="menu-header-text">Settings</span>
                </li>

                <i class='bx bx-cart-add' ></i>

                <li class="menu-item">
                  <a href="icons-boxicons.html" class="menu-link">
                    <i class="fa-solid fa-users me-3"></i>
                    <div data-i18n="Boxicons">ผู้ใช้งานระบบ</div>
                </a>
                </li>


              </ul>
            </aside> --}}
            <!-- / Menu -->

            <!-- Layout container -->
            <div class="layout-page">
              <!-- Navbar -->

              <nav
                class="layout-navbar container-xxl navbar navbar-expand-xl navbar-detached align-items-center bg-navbar-theme"
                id="layout-navbar"
              >
                <div class="layout-menu-toggle navbar-nav align-items-xl-center me-3 me-xl-0 d-xl-none">
                  <a class="nav-item nav-link px-0 me-xl-4" href="javascript:void(0)">
                    <i class="bx bx-menu bx-sm"></i>
                  </a>
                </div>

                <div class="navbar-nav-right d-flex align-items-center" id="navbar-collapse" style="position: absolute;right: 1%;">
                  <!-- Search -->
                  <div class="navbar-nav align-items-center">
                    {{-- <div class="nav-item d-flex align-items-center">
                      <i class="bx bx-search fs-4 lh-0"></i>
                      <input
                        type="text"
                        class="form-control border-0 shadow-none"
                        placeholder="Search..."
                        aria-label="Search..."
                      />
                    </div> --}}
                  </div>
                  <!-- /Search -->
                  <ul class="navbar-nav flex-row align-items-center ms-auto" >
                    <!-- Place this tag where you want the button to render. -->
                    {{-- <li class="nav-item lh-1 me-3">
                      <a
                        class="github-button"
                        href="https://github.com/themeselection/sneat-html-admin-template-free"
                        data-icon="octicon-star"
                        data-size="large"
                        data-show-count="true"
                        aria-label="Star themeselection/sneat-html-admin-template-free on GitHub"
                        >Star</a
                      >
                    </li> --}}

                    <!-- User -->



                    <input type="text" id="token" value="{{ session('token') }}" hidden>
                    <input type="text" id="role" value="{{ Auth::user()->role->id }}" hidden>
                    <input type="text" id="user_id" value="{{ Auth::user()->id }}" hidden>

                    <li class="nav-item navbar-dropdown dropdown-user dropdown" >
                      <a class="nav-link dropdown-toggle hide-arrow" href="javascript:void(0);" data-bs-toggle="dropdown">
                        <div class="avatar avatar-online">
                          <img src="{{ asset('assets/img/avatars/1.png')}}" alt class="w-px-40 h-auto rounded-circle" />
                        </div>
                      </a>
                      <ul class="dropdown-menu dropdown-menu-end">
                        <li>
                          <a class="dropdown-item" href="#">
                            <div class="d-flex">
                              <div class="flex-shrink-0 me-3">
                                <div class="avatar avatar-online">
                                  <img src="{{ asset('assets/img/avatars/1.png')}}" alt class="w-px-40 h-auto rounded-circle" />
                                </div>
                              </div>
                              <div class="flex-grow-1">
                                <span class="fw-semibold d-block">{{ Auth::user()->name }}</span>
                                <small class="text-muted">{{ Auth::user()->role->name }}</small>
                              </div>
                            </div>
                          </a>
                        </li>
                        <li>
                          <div class="dropdown-divider"></div>
                        </li>
                        <li>
                          <a class="dropdown-item" href="#">
                            <i class="bx bx-user me-2"></i>
                            <span class="align-middle">My Profile</span>
                          </a>
                        </li>
                        <li>
                          <a class="dropdown-item" href="#">
                            <i class="bx bx-cog me-2"></i>
                            <span class="align-middle">Settings</span>
                          </a>
                        </li>
                        {{-- <li>
                          <a class="dropdown-item" href="#">
                            <span class="d-flex align-items-center align-middle">
                              <i class="flex-shrink-0 bx bx-credit-card me-2"></i>
                              <span class="flex-grow-1 align-middle">Billing</span>
                              <span class="flex-shrink-0 badge badge-center rounded-pill bg-danger w-px-20 h-px-20">4</span>
                            </span>
                          </a>
                        </li> --}}
                        <li>
                          <div class="dropdown-divider"></div>
                        </li>
                        <li>
                          <a class="dropdown-item" href="{{ route('logout') }}"
                          onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();">
                            <i class="bx bx-power-off me-2"></i>
                            <span class="align-middle">Log Out</span>
                          </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>


                        </li>
                      </ul>
                    </li>
                    <!--/ User -->
                  </ul>
                </div>
              </nav>

              <!-- / Navbar -->

              <!-- Content wrapper -->
              <div class="content-wrapper">
                <!-- Content -->
                <main class="py-4">
                    <div class="container-xxl">
                        <aside id="layout-menu" class="layout-menu menu-vertical menu bg-menu-theme">
                          <div class="app-brand demo">
                            <a href="#" class="app-brand-link" style="text-decoration: none;">
                              <span class="app-brand-logo demo">

                                <img src="/img/logo_circle.png" alt="" style="width: 35px;">

                              </span>
                              <span class="app-brand-text fw-bolder ms-2">(HR)SirigreenFarm</span>
                            </a>

                            <a href="javascript:void(0);" class="layout-menu-toggle menu-link text-large ms-auto d-block d-xl-none">
                              <i class="bx bx-chevron-left bx-sm align-middle"></i>
                            </a>
                          </div>

                          <div class="menu-inner-shadow"></div>

                          <ul class="menu-inner py-1">
                            <!-- Dashboard -->
                            <li class="menu-item {{ (request()->is('*/home') ? 'active' : '')}}">
                                <a href="{{url("hr/home")}}" class="menu-link" style="text-decoration: none;">
                                    <i class="menu-icon tf-icons bx bx-home-circle"></i>
                                    <div data-i18n="Boxicons">Dashboard</div>
                                </a>
                            </li>

                            <li class="menu-header small text-uppercase" >
                                <!-- <i class="fa-solid fa-bag-shopping"></i> -->
                                <i class="menu-icon tf-icons bx bx-street-view"></i>
                                <span class="menu-header-text">HR</span>
                            </li>

                            <li class="menu-item {{ (request()->is('*/employee/*') ? 'active' : '')}}" >
                                <a href="{{url("hr/employee/list")}}" class="menu-link" style="text-decoration: none;">
                                    <i class="menu-icon tf-icons bx bx-user"></i>
                                    <div data-i18n="Boxicons">พนักงาน</div>
                                </a>
                            </li>
                            <li class="menu-item {{ (request()->is('*/salary/*') ? 'active' : '')}}" >
                                <a href="{{url("hr/salary/list")}}" class="menu-link" style="text-decoration: none;">
                                    <i class="menu-icon tf-icons bx bx-dollar-circle"></i>
                                    <div data-i18n="Boxicons">เงินเดือน</div>
                                </a>
                            </li>




                          </ul>



                        </aside>

                    </div>





                        @yield('content')

                    </div>
                </main>



                <!-- / Content -->

                <!-- Footer -->



                <footer class="content-footer footer bg-footer-theme">
                  <div class="container-xxl d-flex flex-wrap justify-content-between py-2 flex-md-row flex-column">
                    <div class="mb-2 mb-md-0">
                      ©
                      <script>
                          document.write(new Date().getFullYear());
                      </script>
                      , made with ❤️ by
                      <a href="https://themeselection.com" target="_blank" class="footer-link fw-bolder">ThemeSelection</a>
                    </div>
                  </div>
                </footer>
                <!-- / Footer -->

                <div class="content-backdrop fade"></div>
              </div>
              <!-- Content wrapper -->
            </div>
            <!-- / Layout page -->

          </div>

          <!-- Overlay -->
          <div class="layout-overlay layout-menu-toggle"></div>
        </div>
        <!-- / Layout wrapper -->


    </div>





    <!-- Core JS -->
    <!-- build:js assets/vendor/js/core.js -->
    <script src="{{ asset('assets/vendor/libs/jquery/jquery.js')}}"></script>
    <script src="{{ asset('assets/vendor/libs/popper/popper.js')}}"></script>
    {{-- <script src="../../assets/vendor/js/bootstrap.js"></script> --}}
    <script src="{{ asset('assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.js')}}"></script>

    <!-- endbuild -->

    <!-- Vendors JS -->

    <!-- Main JS -->
    <script src="{{ asset('assets/js/main.js')}}"></script>



    <!-- Page JS -->

    <!-- Place this tag in your head or just before your close body tag. -->
    <script async defer src="https://buttons.github.io/buttons.js"></script>


    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.css">

    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.js"></script>

    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.css" integrity="sha512-In/+MILhf6UMDJU4ZhDL0R0fEpsp4D3Le23m6+ujDWXwl3whwpucJG1PEmI3B07nyJx+875ccs+yX2CqQJUxUw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js"
        integrity="sha512-8QFTrG0oeOiyWo/VM9Y8kgxdlCryqhIxVeRpWSezdRRAvarxVtwLnGroJgnVW9/XBRduxO/z1GblzPrMQoeuew=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>


    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

    <script>
        $(document).ready(function() {

            let token = "{{ session('token') }}";


        });
    </script>

  </body>
</html>

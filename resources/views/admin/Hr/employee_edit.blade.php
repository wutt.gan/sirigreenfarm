@extends('admin.Hr.layout')

@section('content')
    <div id="app" class="container">


        <employee-edit
        :data_dropdown={{  json_encode($data) }}
        :data_employee={{  json_encode($employee) }}>
    </employee-edit>

    </div>
@endsection

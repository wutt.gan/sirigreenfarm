@extends('admin.Hr.layout')

@section('content')
    <div id="app" class="container">
        <employee-create :data_dropdown={{  json_encode($data) }}></employee-create>

    </div>
@endsection

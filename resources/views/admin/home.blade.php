@extends('layouts.admin')

@section('content')
    <div>
        <div id="app">
            <menu-admin></menu-admin>


            <router-view></router-view>
        </div>
    </div>
@endsection

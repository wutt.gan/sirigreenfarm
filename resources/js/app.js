/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue').default;


import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

// Import Bootstrap and BootstrapVue CSS files (order is important)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)


import VueRouter from 'vue-router'
Vue.use(VueRouter)

const routes = [
    { path: '/dashboard/', component: require('./components/Admin/DashBoard.vue').default },
    { path: '/offer/price/', component: require('./components/Admin/OfferPrice.vue').default },

    { path: '/product/:type', component: require('./components/Admin/Product/ListProduct.vue').default },
    { path: '/product/create/salad/:type', component: require('./components/Admin/Product/CreateProduct.vue').default },

    {
        path: '/',
        redirect: '/dashboard',
        component: require('./components/Admin/DashBoard.vue').default,
    }
]

const router = new VueRouter({
    routes
})

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('test-01', require('./components/Test01.vue').default);



Vue.component('funtion-product-front', require('./components/FuntionProductFront.vue').default);
Vue.component('funtion-product', require('./components/FuntionProduct.vue').default);


Vue.component('web-show-home', require('./components/MainWebShowHome.vue').default);
Vue.component('from-order-price', require('./components/WebShowHome/FormOrderPrice.vue').default);
Vue.component('show-news', require('./components/WebShowHome/ShowNews.vue').default);

//product
Vue.component('list-salad', require('./components/WebShowHome/ListSalad.vue').default);
Vue.component('list-product-misssveggie', require('./components/WebShowHome/ListProductMissveggie.vue').default);

// admin
Vue.component('menu-admin', require('./components/Admin/MenuAdmin.vue').default);
Vue.component('offer-price-list', require('./components/Admin/OfferPrice.vue').default);

//hr
Vue.component('employee-list', require('./components/Admin/Hr/EmployeeList.vue').default);
Vue.component('employee-create', require('./components/Admin/Hr/EmployeeCreate.vue').default);
Vue.component('employee-edit', require('./components/Admin/Hr/EmployeeEdit.vue').default);



/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    router
});


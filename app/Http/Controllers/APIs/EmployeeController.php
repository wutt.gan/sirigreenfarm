<?php

namespace App\Http\Controllers\APIs;

use App\Http\Controllers\Controller;
use App\Models\Employee;
use App\Models\Remuneration;
use Illuminate\Http\Request;
use Laravel\Ui\Presets\React;

class EmployeeController extends Controller
{
    protected $employee;
    protected $remuneration;

    public function __construct(Employee $employee,Remuneration $remuneration) {
        $this->employee = $employee;
        $this->remuneration = $remuneration;
    }

    public function save(Request $request)
    {
        $data = [];
        if ( $request->file('file') ) {
            $product = save_pdf($request->file('file'),('images/employee/'));
            $data = ['card_pdf' => $product];
        };


        $data += [
                'title' => $request->get('title_name'),
                'id_type_place' => $request->get('type_place'),
                'id_position' => $request->get('position'),
                'id_bank' => $request->get('bank'),
                'code_id' => $request->get('code'),
                'full_name_th' => $request->get('name_th'),
                'full_name_en' => $request->get('name_en'),
                'nick_name' => $request->get('name_en'),
                'email' => $request->get('email'),
                'tel' => $request->get('tel'),
                'line' => $request->get('tel'),
                'date_work' => $request->get('date_work'),
                'code_bank' => $request->get('code_bank'),
            ];


        $new_employee = $this->employee->create($data);

        $this->remuneration->create([
            'id_employee' => $new_employee->id,
            'id_user_approve' => 1,
            'daily_wages' => $request->get('daily_wages'),
            'monthly_saraly' => $request->get('monthly_saraly'),
            'remark' => 'new',
        ]);



        return $new_employee;
    }


    public function listTable(){
        return datatables()->collection(Employee::where('record_status',1)->with('typePlace')->with('positionEmployee')->get())->toJson();
    }
}

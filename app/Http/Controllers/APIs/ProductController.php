<?php

namespace App\Http\Controllers\APIs;

use App\Http\Controllers\Controller;
use App\Models\BrandProduct;
use App\Models\Products;
use App\Models\TypeBrand;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    protected $products;

    public function __construct(Products $products) {
        $this->products = $products;
    }

    public function save(Request $request)
    {
        // dd($request->all());
        if ( $request->file('file') ) {
            $product = save_image($request->file('file'),2000,('product/'));
        };

        $new_product = $this->products->create([
            'id_product_type' => $request->get('type_product'),
            'id_product_brand' => $request->get('type_brand'),
            'id_product_unit' => $request->get('type_unit'),
            'code' => $request->get('code'),
            'name_th' => $request->get('name_th'),
            'name_en' => $request->get('name_en'),
            'amount' => $request->get('amount'),
            'price' => $request->get('price'),
            'image' => $product,
            'status' => $request->get('status'),
            'discription' => $request->get('discription'),
        ]);

        return response()->json($new_product);
    }

    public function edit(Request $request)
    {
        $get_product = $this->products->find($request->get('id_product'));

        $product = null;

        if ( $request->file('file') ) {
            $destinationPath = 'product/';
            File::delete($destinationPath.$get_product->image);

            // Storage::delete($get_product->image);
            $product = save_image($request->file('file'),2000,('product/'));
        };

        $new_product = $this->products->where('id',$get_product->id)->update([
            'id_product_unit' => $request->get('type_unit'),
            'id_product_brand' => $request->get('type_brand'),
            'code' => $request->get('code'),
            'name_th' => $request->get('name_th'),
            'name_en' => $request->get('name_en'),
            'amount' => $request->get('amount'),
            'price' => $request->get('price'),
            'discription' => $request->get('discription'),
            'image' => $product == null ? $get_product->image : $product,
            'status' => $request->get('status'),
        ]);

        return response()->json($new_product);
    }

    public function listTable($id){

        return datatables()->collection(Products::where('id_product_type',$id)->with('typeUnit')->get())->toJson();
    }

    public function list(Request $request){

        $brand_id = null;

        // dd($request->all);
        if ($request->type_brand == null) {

            $brand = TypeBrand::where('id_product_type', $request->id_type)->with('brandProduct')->first();

            $brand_id = $brand->brandProduct->id;
        }else{
            $brand_id = $request->type_brand;
        }

        $all_count = Products::where([['id_product_type',$request->id_type],['id_product_brand',$brand_id]])->with('typeUnit')->count();

        $data_product = Products::where([['id_product_type',$request->id_type],['id_product_brand',$brand_id]])
        ->with('typeUnit')
        ->skip($request->take*($request->to - 1))
        ->take($request->take)
        ->get();

        $data = [
            'data_product' => $data_product,
            'all_count' => $all_count,
            'take' => $request->take,
            'to' => $request->to,
            'brand' => $brand_id,
        ];

        return response()->json($data);
    }

    public function showEdit($id)
    {
        $product = $this->products->where('id',$id)->with('typeProduct')->get();

        return response()->json($product[0]);
    }
}

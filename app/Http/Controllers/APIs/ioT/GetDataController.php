<?php

namespace App\Http\Controllers\APIs\ioT;

use App\Http\Controllers\Controller;
use App\Models\NodeMcuButton;
use App\Models\NodeMcuKey;
use App\Models\NodeMcuSenser;
use App\Models\NodeMcuSenserValue;
use Illuminate\Http\Request;

use function App\Helpers\CheckKey;

class GetDataController extends Controller
{
    public function statusButton($key,$button,$id_node)
    {
        $check_key = CheckKey($id_node,$key);
        if ($check_key != 0) {
            $button = NodeMcuButton::where([['id_node',$id_node],['code_button',$button],['record_status',1]])->first();

            $data = [
                'button' => $button->status,
                'status' => 200,
            ];

            return response()->json($data);
        }else{
            $data = [
                'text' => 'not product key',
                'status' => 500,
            ];
            return response()->json($data);
        }
    }

    public function statusSenser($key,$senser,$id_node)
    {
        $check_key = CheckKey($id_node,$key);
        if ($check_key != 0) {
            $senser_value = NodeMcuSenserValue::whereHas('senser', function ($query) use ($senser) {
                $query->where('code_senser', $senser);
            })->get();

            return response()->json($senser_value);
        }else{
            return 'not product key';
        }

    }
}

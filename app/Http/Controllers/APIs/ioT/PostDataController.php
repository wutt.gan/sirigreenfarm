<?php

namespace App\Http\Controllers\APIs\ioT;

use App\Http\Controllers\Controller;
use App\Models\NodeMcuButton;
use App\Models\NodeMcuKey;
use App\Models\NodeMcuSenser;
use App\Models\NodeMcuSenserValue;
use App\Models\NodeMcuSenserValueDay;
use Carbon\Carbon;
use Illuminate\Http\Request;

use function App\Helpers\CheckKey;

class PostDataController extends Controller
{
    public function actionButton(Request $request)
    {
        $check_key = CheckKey($request->id_node,$request->key);
        if ($check_key != 0) {
            $button = NodeMcuButton::where([['id_node',$request->id_node],['code_button',$request->code_button],['record_status',1]])->first();

            $status = $request->status;

            $button->update(['status' => $status]);

            return response()->json($button->status);
        }else{
            return 'not product key';
        }
    }

    public function sendData(Request $request)
    {
        // Set the timezone to Thailand
        $timezone = 'Asia/Bangkok';
        // Get the current time in the Thai timezone
        $currentDateTime = Carbon::now($timezone);
        // Format the current time as per your requirements
        $now_hour = $currentDateTime->format('H');
        $formattedDateTime = $currentDateTime->format('Y-m-d 00:00:00');


        $check_key = CheckKey($request->id_node,$request->key);
        if ($check_key != 0) {
            $senser = NodeMcuSenser::where([['id_node',$request->id_node],['code_senser',$request->code_senser],['record_status',1]])->first();

            if ($senser) {

                // อัตเดตข้อมูลรายวัน
                if($now_hour == "17"){
                    if(!NodeMcuSenserValueDay::whereDate('date',$formattedDateTime)->where('id_node_senser',$senser->id)->first()){
                        $avg_value = NodeMcuSenserValue::where('id_node_senser',$senser->id)->avg('value');
                        NodeMcuSenserValueDay::create([
                            'id_node_senser' => $senser->id,
                            'date' => $formattedDateTime,
                            'value' => $avg_value,
                        ]);
                    }
                }

                NodeMcuSenserValue::create([
                    'id_node_senser' => $senser->id,
                    'value' => $request->value,
                ]);

                $recordsToDelete = NodeMcuSenserValue::where('id_node_senser',$senser->id)->latest()->get();
                if ($recordsToDelete->count() > 10) {
                    $recordsToDelete->slice(10)->each->delete();
                }

            }else{
                return 'not found senser';
            }

            return response()->json(200);
        }else{
            return 'not product key';
        }
    }
}

<?php

namespace App\Http\Controllers\APIs;

use App\Http\Controllers\Controller;
use App\Models\BrandProduct;
use Illuminate\Http\Request;

class BrandProductController extends Controller
{
    protected $brandProducts;

    public function __construct(BrandProduct $brandProducts) {
        $this->brandProducts = $brandProducts;
    }

    public function list()
    {
        $data = $this->brandProducts->all();
        return response()->json($data);
    }

    public function listFront($type_product)
    {
        $data = $this->brandProducts
        ->whereHas('typeProducts', function($q) use($type_product) {
            $q->where('id_product_type', $type_product);
        })->get();
        // dd($data);
        return response()->json($data);
    }
}

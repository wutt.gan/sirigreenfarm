<?php

namespace App\Http\Controllers\APIs;

use App\Http\Controllers\Controller;
use App\Models\TypeProducts;
use Illuminate\Http\Request;

class TypeProductController extends Controller
{
    protected $typeProducts;

    public function __construct(TypeProducts $typeProducts) {
        $this->typeProducts = $typeProducts;
    }

    public function list()
    {
        $data = $this->typeProducts->all();
        return response()->json($data);
    }

    public function getTypeProduct($id)
    {
        $data = $this->typeProducts->find($id);
        return response()->json($data);
    }
}

<?php

namespace App\Http\Controllers\APIs;

use App\Http\Controllers\Controller;
use App\Models\UnitProducts;
use Illuminate\Http\Request;

class UnitController extends Controller
{
    protected $unitProducts;

    public function __construct(UnitProducts $unitProducts) {
        $this->unitProducts = $unitProducts;
    }

    public function list()
    {
        $data = $this->unitProducts->all();
        return response()->json($data);
    }
}

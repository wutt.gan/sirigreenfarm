<?php

namespace App\Http\Controllers\APIs;

use App\Http\Controllers\Controller;
use App\Models\FormOrderPrice;
use Illuminate\Http\Request;
use DataTables;

class FromOrderController extends Controller
{
    protected $formOrderPrice;
    public function __construct(FormOrderPrice $formOrderPrice) {
        $this->formOrderPrice = $formOrderPrice;
    }

    public function create(Request $request)
    {
        $new_order = $this->formOrderPrice->create([
            'fullname' => $request->get('fullname'),
            'email' => $request->get('email'),
            'line' => $request->get('line'),
            'tel' => $request->get('tel'),
            'note' => $request->get('note'),
        ]);

        return response()->json($new_order);
    }

    public function list(){
        return datatables()->collection(FormOrderPrice::all())->toJson();
    }

    public function confirm(Request $request)
    {
        $new_order = $this->formOrderPrice->where('id',$request->id)->update([
            'remark' => $request->get('remark'),
            'id_user_confirm' => $request->get('user_id'),
            'status' => 2,
        ]);
        return response()->json($new_order);
    }
}

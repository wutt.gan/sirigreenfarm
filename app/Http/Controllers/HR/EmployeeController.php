<?php

namespace App\Http\Controllers\HR;

use App\Http\Controllers\Controller;
use App\Models\Bank;
use App\Models\Employee;
use App\Models\PositionEmployee;
use App\Models\Remuneration;
use App\Models\TypePlace;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use function App\Helpers\getDropDown;

class EmployeeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function list()
    {
        return view('admin.Hr.employee');
    }


    public function create()
    {
        return view('admin.Hr.employee_create',[
            'data'=> getDropDown()
        ]);
    }

    public function edit($id)
    {
        $get_employee = Employee::find($id);


        $salary = Remuneration::where('id_employee',$id)->orderBy('id','desc')->first();
        return view('admin.Hr.employee_edit',[
            'employee' => [$get_employee,$salary],
            'data'=> getDropDown()
        ]);
    }

    public function loadPDF($id)
    {
        $get_employee = Employee::find($id);
        return Storage::download('images/employee'.'/'.$get_employee->card_pdf);

    }
}

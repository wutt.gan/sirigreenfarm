<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FromOrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function list()
    {
        return view('admin.OfferPrice.offer_price_home');
    }
}

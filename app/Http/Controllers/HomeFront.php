<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeFront extends Controller
{
    public function homeFarm()
    {
        return view('web_show.home_farm',[
        ]);
    }
    public function homeMissveggie()
    {
        return view('web_show.home_missveggie',[
        ]);
    }
    public function homeCfire()
    {
        return view('web_show.home_cfire',[
        ]);
    }
    public function homeSupplier()
    {
        return view('web_show.home_supplier',[
        ]);
    }
}

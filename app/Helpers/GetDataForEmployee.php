<?php

namespace App\Helpers;

use App\Models\Bank;
use App\Models\PositionEmployee;
use App\Models\TypePlace;

function getDropDown()
{
    $type_place = TypePlace::where('record_status',1)->get();
    $bank = Bank::where('record_status',1)->get();
    $position_employee = PositionEmployee::where('record_status',1)->get();
    $data = [
        'type_place'=>$type_place,
        'bank'=>$bank,
        'position_employee'=>$position_employee
    ];
    return $data;
}

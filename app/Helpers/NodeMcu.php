<?php

namespace App\Helpers;

use App\Models\NodeMcuKey;

function CheckKey($id_node,$key)
{
    $check_key = NodeMcuKey::where([['id_node',$id_node],['id_key',$key]])->count();
    return $check_key;
}

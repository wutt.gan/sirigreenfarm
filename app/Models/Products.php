<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    use HasFactory;

    protected $fillable = [
        'id_product_type',
        'id_product_brand',
        'id_product_unit',
        'code',
        'name_th',
        'name_en',
        'amount',
        'price',
        'image',
        'discription',
        'status',
        'record_status',
    ];

    public function typeProduct()
    {
        return $this->belongsTo(TypeProducts::class, 'id_product_type', 'id');
    }

    public function brandProduct()
    {
        return $this->belongsTo(BrandProduct::class, 'id_product_brand', 'id');
    }

    public function typeUnit()
    {
        return $this->belongsTo(UnitProducts::class, 'id_product_unit', 'id');
    }
}


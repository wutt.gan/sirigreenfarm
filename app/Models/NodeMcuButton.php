<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NodeMcuButton extends Model
{
    use HasFactory;

    protected $fillable = [
        'id_node',
        'code_button',
        'description',
        'status',
        'record_status',
    ];

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Remuneration extends Model
{
    use HasFactory;

    protected $fillable = [
        'id_employee',
        'id_user_approve',
        'daily_wages',
        'monthly_saraly',
        'remark',
    ];
}

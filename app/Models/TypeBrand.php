<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TypeBrand extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'id_product_type',
        'id_product_brand',
    ];

    public function typeProduct()
    {
        return $this->belongsTo(TypeProducts::class, 'id_product_type', 'id');
    }

    public function brandProduct()
    {
        return $this->belongsTo(BrandProduct::class, 'id_product_brand', 'id');
    }
}

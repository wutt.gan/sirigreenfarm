<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FormOrderPrice extends Model
{
    use HasFactory;

    protected $fillable = [
        'fullname',
        'email',
        'line',
        'tel',
        'note',
        'status',
        'remark',
        'id_user_confirm',
        'record_status',
    ];
}




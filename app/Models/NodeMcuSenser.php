<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NodeMcuSenser extends Model
{
    use HasFactory;

    protected $fillable = [
        'id_node',
        'code_senser',
        'description',
        'value',
        'record_status',
    ];

    public function valueSenser()
    {
        return $this->hasMany(NodeMcuSenserValue::class, 'id', 'id_node_senser');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NodeMcuSenserValue extends Model
{
    use HasFactory;

    protected $fillable = [
        'id_node_senser',
        'value',
        'record_status',
    ];

    public function senser()
    {
        return $this->belongsTo(NodeMcuSenser::class, 'id_node_senser', 'id');
    }
}

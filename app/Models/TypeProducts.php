<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TypeProducts extends Model
{
    use HasFactory;

    protected $fillable = [
        'id_product_type',
        'code',
        'name_th',
        'record_status',
    ];

    public function typeBrand()
    {
        return $this->belongsToMany(TypeBrand::class,'type_brands','id_product_brand','id_product_brand');
    }
}

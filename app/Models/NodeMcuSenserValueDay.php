<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NodeMcuSenserValueDay extends Model
{
    use HasFactory;

    protected $fillable = [
        'id_node_senser',
        'value',
        'date',
        'record_status',
    ];

    public function senser()
    {
        return $this->belongsTo(NodeMcuSenser::class, 'id_node_senser', 'id');
    }
}

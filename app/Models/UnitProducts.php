<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UnitProducts extends Model
{
    use HasFactory;

    protected $fillable = [
        'id_product_type',
        'code',
        'name_th',
        'record_status',
    ];
}

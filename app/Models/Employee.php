<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'id_type_place',
        'id_position',
        'id_bank',
        'code_id',
        'full_name_th',
        'full_name_en',
        'nick_name',
        'email',
        'tel',
        'line',
        'date_work',
        'code_bank',
        'card_pdf',
        'status',
        'record_status',
    ];

    public function typePlace()
    {
        return $this->belongsTo(TypePlace::class, 'id_type_place', 'id');
    }

    public function positionEmployee()
    {
        return $this->belongsTo(PositionEmployee::class, 'id_position', 'id');
    }

    public function remuneration()
    {
        return $this->hasMany(Remuneration::class, 'id', 'id_employee');
    }


}

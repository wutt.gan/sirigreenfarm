<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BrandProduct extends Model
{
    use HasFactory;

    public function typeProducts()
    {
        return $this->belongsToMany(TypeProducts::class,'type_brands','id_product_brand','id_product_type');
    }
}

<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//front
Route::get('/', function () {
    return view('web_show.home');
})->name('index');

Route::get('/farm', [App\Http\Controllers\HomeFront::class, 'homeFarm'])->name('home_farm');
Route::get('/missveggie', [App\Http\Controllers\HomeFront::class, 'homeMissveggie'])->name('home_missveggie');
Route::get('/cfire', [App\Http\Controllers\HomeFront::class, 'homeCfire'])->name('home_cfire');
Route::get('/supplier', [App\Http\Controllers\HomeFront::class, 'homeSupplier'])->name('home_supplier');


//back
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// HR
Route::prefix('hr')->group(function () {
    Route::get('/home', [App\Http\Controllers\HR\HomeController::class, 'home']);

    Route::get('/employee/list', [App\Http\Controllers\HR\EmployeeController::class, 'list']);
    Route::get('/employee/create', [App\Http\Controllers\HR\EmployeeController::class, 'create']);
    Route::get('/employee/edit/{id}', [App\Http\Controllers\HR\EmployeeController::class, 'edit']);


    Route::get('/salary/list', [App\Http\Controllers\HR\SalaryController::class, 'list']);
});

// Route::get('/{vue_capture?}', [App\Http\Controllers\HomeController::class, 'index'])->where('vue_capture', '*');

// Route::get('/{all}', [App\Http\Controllers\HomeController::class, 'index'])->where(['all' => '.*']);

// Route::any('/spa/{all}', function () {
//     return view('index');
// })
// ->where(['all' => '.*']);


// Route::get('/{vue_capture?}', function () {
//     [App\Http\Controllers\HomeController::class, 'index'];
//   })->where('vue_capture', '[\/\w\.-]*');


Route::prefix('formorder')->group(function () {
    Route::get('/list', [App\Http\Controllers\FromOrderController::class, 'list'])->name('formorder.list');
});

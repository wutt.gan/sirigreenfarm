<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//forshow
Route::post('/formorder/create', [App\Http\Controllers\APIs\FromOrderController::class, 'create']);

Route::prefix('product_show')->group(function () {
    Route::post('/list', [App\Http\Controllers\APIs\ProductController::class, 'list']);
});


Route::post('/login', [App\Http\Controllers\API\AuthController::class, 'login']);

Route::get('/brandproduct/{type_product}', [App\Http\Controllers\APIs\BrandProductController::class, 'listFront']);






//foradmin
//Protecting Routes
Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::get('/profile', function(Request $request) {
        return auth()->user();
    });
    // API route for logout user
    Route::post('/logout', [App\Http\Controllers\API\AuthController::class, 'logout']);


    Route::prefix('formorder')->group(function () {
        Route::get('/list', [App\Http\Controllers\APIs\FromOrderController::class, 'list']);
        Route::post('/confirm', [App\Http\Controllers\APIs\FromOrderController::class, 'confirm']);
    });

    Route::get('/unit', [App\Http\Controllers\APIs\UnitController::class, 'list']);
    Route::get('/brandproduct', [App\Http\Controllers\APIs\BrandProductController::class, 'list']);
    Route::get('/typeproduct', [App\Http\Controllers\APIs\TypeProductController::class, 'list']);
    Route::get('/typeproduct/{id}', [App\Http\Controllers\APIs\TypeProductController::class, 'getTypeProduct']);

    Route::prefix('product')->group(function () {
        Route::get('/show/edit/{id}', [App\Http\Controllers\APIs\ProductController::class, 'showEdit']);
        Route::get('/list/{type}', [App\Http\Controllers\APIs\ProductController::class, 'listTable']);
        Route::post('/save', [App\Http\Controllers\APIs\ProductController::class, 'save']);
        Route::post('/edit', [App\Http\Controllers\APIs\ProductController::class, 'edit']);
    });

    Route::prefix('employee')->group(function () {
        Route::get('/list', [App\Http\Controllers\APIs\EmployeeController::class, 'listTable']);
        Route::post('/save', [App\Http\Controllers\APIs\EmployeeController::class, 'save']);
    });

});


// api for ioT
    Route::prefix('sirigreen/ioT')->group(function () {

        // get data
        Route::prefix('get')->group(function () {
            Route::get('/status/button/{key}/{button}/{id_node}', [App\Http\Controllers\APIs\ioT\GetDataController::class, 'statusButton']);
            Route::get('/status/senser/{key}/{senser}/{id_node}', [App\Http\Controllers\APIs\ioT\GetDataController::class, 'statusSenser']);
        });

        // post data
        Route::prefix('post')->group(function () {
            Route::post('/action/button', [App\Http\Controllers\APIs\ioT\PostDataController::class, 'actionButton']);
            Route::post('/data/senser', [App\Http\Controllers\APIs\ioT\PostDataController::class, 'sendData']);
        });
    });
